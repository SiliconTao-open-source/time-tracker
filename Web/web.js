var LastTab = 0;

function addLoadEvent(func) {
  var oldonload = window.onload;
  if (typeof window.onload != 'function') {
    window.onload = func;
  } else {
    window.onload = function() {
      if (oldonload) {
        oldonload();
      }
      func();
    }
  }
}

function ChangeTab(ViewTab)
{
  // alert("ChangeTab("+ViewTab+")");

  // $("#Tab"+LastTab).css({ backgroundColor: '#898B5E', borderBottom: 'none' });    
  if(LastTab > 0)
  {
    Tlt = document.getElementById("Tab"+LastTab);
    Tlt.style.backgroundColor = "#898B5E";
    Tlt.style.borderBottom = "none";

    Clt = document.getElementById("content"+LastTab);
    Clt.style.display = "none";
  }

  Ndt = document.getElementById("Tab"+ViewTab);
  Ndt.style.backgroundColor = "#DDFAE9";
  Ndt.style.borderBottom = '3px solid #DDFAE9'

  Nct = document.getElementById("content"+ViewTab);
  Nct.style.display = "block";        
  LastTab = ViewTab;
}
