var MainTimer;
var ProgressBarCounter = 0;
var MyDevice = "";

function LoadTargetDiv(TrgDiv, Url) {
   var GetHttp = new XMLHttpRequest();
   GetHttp.onreadystatechange = function(){OnTrgDivLoaded(GetHttp, TrgDiv)}
   GetHttp.open("GET", Url, true);
   GetHttp.send(null);
}

function OnTrgDivLoaded(GetHttp, TrgDiv) {
   if((GetHttp.readyState == 4) && (GetHttp.status == 200))
   {
      TrgElm = document.getElementById(TrgDiv);
      if(TrgElm != undefined)
      {
         //alert("loading DIV ID "+TrgDiv);
         TrgElm.innerHTML = GetHttp.responseText;
      }
   }
}

// In or Out status button
function GetStatus() {
	var GetHttp = new XMLHttpRequest();
	GetHttp.onreadystatechange = function(){SetStatus(GetHttp)}
	GetHttp.open("GET", "/InStatus", true);
	GetHttp.send(null);
}

function SetStatus(GetHttp) {
	if((GetHttp.readyState == 4) && (GetHttp.status == 200)) {
		IdBtn = document.getElementById("InOutButton");
		if(IdBtn != undefined) {
			// console.log("Got back '" + GetHttp.responseText + "'");
			if(GetHttp.responseText == 'Out') {
				IdBtn.value = "Log In";
				$("#ContinueButton").show();
				$('#RecordButton').hide();
			} else {
				IdBtn.value = "Log Out";
				$("#ContinueButton").hide();
				$('#RecordButton').show();
			}
		}
	}
	LoadTargetDiv("ActivityHistory", "/ActivityHistory");
}

// I don't want this button on the page. It is only to debug the login page faster and I am done with that.
//function Logout () {
//	$.removeCookie("session");
//	location.reload();
//}

function ServerLog(LogMessage) {
	CallCommand("/ServerLog?LogMessage="+LogMessage, function() { console.log('message sent'); } );
}

function ContinueActivity(ActivityId) {
	ActivityText = $("#Activity_"+ActivityId).text();
	if (confirm("Continue Activity: "+ActivityText)) {
		CallCommand("/ContinueActivity?ActivityId="+ActivityId, () => {
			$("#SelectedProject").val("");
			$("#ActivityInput").val("");
			PageSetup();
		});
	}
}

function Continuing() {
	CallCommand("/Continuing", PageSetup);
}

function CallCommand(Url, CallFunction) {
	var GetHttp = new XMLHttpRequest();
	GetHttp.onreadystatechange = function(){CommandReturn(GetHttp, CallFunction)}
	GetHttp.open("GET", Url, true);
	GetHttp.send(null);
}

function CommandReturn(GetHttp, CallFunction) {
	if((GetHttp.readyState == 4) && (GetHttp.status == 200)) {
		CallFunction();
	}
}

function LogInOut() {
	if($("#InOutButton").attr('value') == "Log In") {
		// $("#InOutButton").attr('value', 'Log Out');
		NewStatus = "In";
	} else {
		$("#InOutButton").attr('value', 'Log In');
		NewStatus = "Out";
	}
	var GetHttp = new XMLHttpRequest();
	GetHttp.onreadystatechange = function(){SetStatus(GetHttp)}
	GetHttp.open("GET", "/ChangeStatus?new="+NewStatus, true);
	GetHttp.send(null);  
}

var ProjectJsonList;

function PageSetup() {
	GetStatus();

	$.getJSON("/JsonProjectList", function(jsonData){
		ProjectJsonList = jsonData;
		UpdateProjectListSelect();
	});
	

	LoadTargetDiv("TimeToday", "/TimeToday");
	LoadTargetDiv("TimeThisWeek", "/TimeThisWeek");
	LoadTargetDiv("ActivityHistory", "/ActivityHistory");
	if(MyDevice == "") {
		MyDevice = (/android|webos|iphone|ipad|ipod|blackberry|iemobile|opera mini/i.test(navigator.userAgent.toLowerCase()));
	 	// MyDevice = $.browser.device;
	 	ServerLog("Device="+ MyDevice);
	}

	ResetTimer();
}

function UpdateProjectListSelect() {
	// console.log("UpdateProjectListSelect");
	ProgressBarCounter = 0;
	
	var TextValue = $("#SelectedProject").val();

	var rgxp = new RegExp(TextValue, "gi");
	$('#ProjectSelector').empty();
	$.each(ProjectJsonList, function(pId, Project) {
		if(Project.match(rgxp)) {
			//console.log("Select Project = "+pId+", "+Project);
			$('#ProjectSelector').append("<option>"+Project+"</option>");
		}
	});
}

function SearchActivity() {
	ProgressBarCounter = 0;	
	var FilterValue = $("#ActivityInput").val();
	LoadTargetDiv("ActivityHistory", "/ActivityHistory?filterActivity="+FilterValue);
}

function ResetTimer() {
	var BarWidth = 60;
	clearTimeout(MainTimer);
	ProgressBarCounter++;
	if( ProgressBarCounter > (BarWidth - 1) ) {
		ProgressBarCounter = 0;
		MainTimer = setTimeout(PageSetup, 1000);
	} else {
		MainTimer = setTimeout(ResetTimer, 1000);
	}
	// console.log("[" + Array(UslessBarCounter).join('-') + "]");
	$("#ProgressBar").html("[" + Array(ProgressBarCounter).join('*') + Array(BarWidth-ProgressBarCounter).join("_")+ "]");
}

function Record() {
	console.log('called selectAction');
	CallCommand("/RecordActivity?SelectedProject="+$("#SelectedProject").val()+"&ActivityInput="+$("#ActivityInput").val(), () => {
		$("#SelectedProject").val("");
		$("#ActivityInput").val("");
		PageSetup();
	});
}