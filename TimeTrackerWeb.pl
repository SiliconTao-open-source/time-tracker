#!/usr/bin/perl -w


# =============================================================================
#
#    Copyright (C) 2019 Silicon Tao Technology Systems Inc.
#    E-mail:  support@SiliconTao.com
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#=============================================================================

# Twiggy & plackup
# http://search.cpan.org/~miyagawa/Plack-1.0034/script/plackup
# http://search.cpan.org/~miyagawa/Plack-1.0034/lib/Plack/Request.pm

use warnings;
use strict;
use DateTime;
use DateTime::Format::Strptime qw( );
use Fcntl qw(:flock);
use Sys::Hostname;
use POSIX;
use TimeTracker;
use Twiggy::Server;
use Plack::Request;
use Plack::Response;
# use Plack::Session::State;
use HTML::Entities;
use JSON;
# use HTTP::Headers::Fast;
#use HTML::HTML5::Parser;
#use HTML::HTML5::Writer;

#my $template = 'HTML::HTML5::Parser'->load_html(IO => \*DATA);
#my $writer   = 'HTML::HTML5::Writer'->new(markup => 'html', polyglot => 1);

# the PSGI app itself
my $app = sub {
	my $req = 'Plack::Request'->new(shift);
	my $res = 'Plack::Response'->new(200);

	my $UserIdSet = "no";
	my $UserType = "no";

	# print "req->request_uri = ".$req->request_uri."\tmethod=".$req->method."\n";
	# print "req->method = '".$req->method."'\n";
	#if ( (defined $req->session) && (defined $req->session->{"session"}) ) {
	#	print "req->session->userid='".$req->session()->{"session"}."'\n";
	#	if($req->session()->{"session"} eq $req->cookies()->{"session"}) {
	if ( defined($req->cookies()->{"session"}) ) {
		#print "req->cookies->{session}='".$req->cookies()->{"session"}."'\n";

		if( ($req->cookies()->{"session"} eq "admin") || ($req->cookies()->{"session"} eq "guest") ) {
			$UserType = $req->cookies()->{"session"};
			$res->cookies->{"session"} = { value => $UserType, path => "/" };
			$UserIdSet = "yes";
		}
	}
	if ($UserIdSet ne "yes" ) {
		if ($req->method eq 'POST') {
			my $ValidUser = "no";
			print "req->request_uri = '".$req->request_uri."'\n";
			print "username = '".$req->parameters()->{'username'}."'\n";
			print "password = '".$req->parameters()->{'password'}."'\n";
			if($dbh->selectall_arrayref("SELECT value FROM settings WHERE name = 'LoginName';")->[0][0] eq $req->parameters()->{'username'}) {
				if($dbh->selectall_arrayref("SELECT value FROM settings WHERE name = 'LoginPass';")->[0][0] eq $req->parameters()->{'password'}) {
					$ValidUser = "yes";
					$UserType = "admin";
				}
			}
			if($ValidUser eq "no") {
				if($dbh->selectall_arrayref("SELECT value FROM settings WHERE name = 'ViewName';")->[0][0] eq $req->parameters()->{'username'}) {
					if($dbh->selectall_arrayref("SELECT value FROM settings WHERE name = 'ViewPass';")->[0][0] eq $req->parameters()->{'password'}) {
						$ValidUser = "yes";
						$UserType = "guest";
					}
				}
			}
			if($ValidUser eq "no") {
				$res->body("Access denied!");
			} else {
				$res->cookies->{"session"} = { value => $UserType, path => "/" };
				# $req->session->{"session"} = $ValidUser;
				$res->body( read_file( "Web/web.html" ));
			}
		} else {			
			$req->request_uri =~ m/login/ && $res->body( read_file( "Web".$req->request_uri ));
			$req->request_uri =~ m/mobile.css/ && MobileStyleSheets($req, $res);
			$req->request_uri eq "/" && $res->body( read_file( "Web/login.html" ));
		}
	} else { 
		# print "req->request_uri = ".$req->request_uri."\tmethod=".$req->method."\n";
		Log($req);
		if ($req->method eq 'POST') {
			#$insert->execute(map $req->parameters->{$_}, qw( name age ));
			#   $res->redirect( $req->base );
			$res->body('POST');
		} else {
			#my $page  = $template->cloneNode(1);
			#my $table = $page->getElementsByTagName('table')->get_node(1);
			#$select->execute;
			#while (my @row = $select->fetchrow_array) {
			#   my $tr = $table->addNewChild($table->namespaceURI, 'tr');
			#   $tr->appendTextChild(td => $_) for @row;
			#}
			# $res->body( $writer->document($page) );
			my $RequestFile = $req->request_uri;
			if($RequestFile eq "/") {
				$RequestFile = "/web.html";
			}
			if( -f "Web".$RequestFile) {
				# print "Serve content 'Web".$RequestFile."'\n";
				$res->body( read_file( "Web".$RequestFile ));
			} else {
				$req->request_uri =~ m/InStatus/ && InStatus($res);
				$req->request_uri =~ m/ListProjects/ && ListProjects($req, $res);
				$req->request_uri =~ m/ActivityHistory/ && ActivityHistory($req, $res);
				$req->request_uri =~ m/ChangeStatus/ && ChangeStatus($req, $res);
				$req->request_uri =~ m/TimeToday/ && TimeToday($req, $res);
				$req->request_uri =~ m/TimeThisWeek/ && TimeThisWeek($req, $res);
				$req->request_uri =~ m/ContinueActivity/ && ContinueActivity($req, $res);
				$req->request_uri =~ m/mobile.css/ && MobileStyleSheets($req, $res);
				$req->request_uri =~ m/Continuing/ && Continuing($req, $res);
				$req->request_uri =~ m/JsonProjectList/ && JsonProjectList($res);
				$req->request_uri =~ m/RecordActivity/ && RecordActivity($req, $res);				
			}
		}
	}
	$res->finalize;
};

sub Continuing {
	my $Request = shift;
	my $Response = shift;	
	my $LastId = $dbh->selectall_arrayref("SELECT MAX(id) FROM timetrack;")->[0][0];
	$dbh->prepare("DELETE FROM timetrack WHERE id=?;")->execute($LastId);
	$LastId = $dbh->selectall_arrayref("SELECT MAX(id) FROM timetrack WHERE project != 'PUNCH';")->[0][0];
	my $dt = DateTime->now();
	my $Now = $dt->epoch();
	my $SqlCmd = "INSERT INTO timetrack (id, project, activity, time) SELECT MAX(id)+1, project, activity, $Now FROM timetrack WHERE id=$LastId;";
	print "$SqlCmd\n";
	$dbh->prepare($SqlCmd)->execute();
	Calculations();
	ResetFailedToEnterCount();
}

sub ContinueActivity {
	my $Request = shift;
	my $Response = shift;
	my $ActivityID = $Request->param('ActivityId'); 
	my $dt = DateTime->now();
	my $Now = $dt->epoch();
	my $NextId = $dbh->selectall_arrayref("SELECT MAX(id)+1 FROM timetrack;")->[0][0];
	$dbh->prepare("INSERT INTO timetrack (id, project, activity, time) SELECT ?, project, activity, ? FROM timetrack WHERE id=?;")->execute($NextId, $Now, $ActivityID);
	Calculations();
	ResetFailedToEnterCount();
}

sub RecordActivity {
	my $Request = shift;
	my $Response = shift;
	my $ActivityDescription = $Request->param('ActivityInput'); 
	my $SelectedProject = $Request->param('SelectedProject'); 
	my $dt = DateTime->now();
	my $Now = $dt->epoch();
	my $NextId = $dbh->selectall_arrayref("SELECT MAX(id)+1 FROM timetrack;")->[0][0];
	$dbh->prepare("INSERT INTO timetrack (id, project, activity, time) SELECT MAX(id)+1, ?, ?, ? FROM timetrack;")->execute($SelectedProject, $ActivityDescription, $Now);
	Calculations();
	ResetFailedToEnterCount();
	# ("/RecordActivity?SelectedProject="+$("#SelectedProject").val()+"&"+$("#ActivityInput").val()
}

sub TimeToday {
	my $Request = shift;
	my $Response = shift;	
	$Response->body(GetTimeToday());
}

sub TimeThisWeek {
	my $Request = shift;
	my $Response = shift;	
	$Response->body(GetTimeThisWeek());
}

sub MobileStyleSheets {
	my $Request = shift;
	my $Response = shift;	
	my $Headers = $Request->headers();
	my $ReturnBody = "<!-- comment -->";
	# $Headers->user_agent =~ m/android|webos|iphone|ipad|ipod|blackberry|iemobile|opera mini/i; 
	$ReturnBody = read_file( "Web/mobile.css" );
	# printf "Response = '$ReturnBody'\n";
	$Response->body($ReturnBody);	
}

sub ChangeStatus {
	my $Request = shift;
	my $Response = shift;	
	my $NewStatus = $Request->param('new');
	# print "new status = '".$NewStatus."'\n";
	SaveInOut($NewStatus);
	$Response->body($NewStatus);
}

sub InStatus {
	my $Response = shift;	
	# $Response->body("header('Content-Type: text/plain');\n\n".InOrOut());
	$Response->body(InOrOut());
}

sub ActivityHistory {
	my $Request = shift;
	my $Response = shift;	
	my $GenBody = "<TABLE CLASS='activity-history'>";
	my $InOrOutValue = InOrOut();
	my $HighlightRows = "off";
	my $ContinueScript = "";

	my $SqlCmd = "SELECT project,activity,id FROM timetrack WHERE activity IS NOT 'In' AND activity IS NOT 'Out' AND activity IS NOT NULL GROUP BY project,activity ORDER BY id DESC LIMIT 50;";
	my $Filter = $Request->param('filterActivity');
	if((defined $Filter) and ($Filter ne "")) {
		$SqlCmd = "SELECT project,activity,id FROM timetrack WHERE activity LIKE '%$Filter%' GROUP BY project,activity ORDER BY id DESC;";
	}

	# print "InOrOutValue='$InOrOutValue'\n";
	if($InOrOutValue eq "In") { $HighlightRows = "low"; }

	$sth = $dbh->prepare($SqlCmd);
	$sth->execute();

	while (my @row = $sth->fetchrow_array ) {
		# print "HighlightRows='$HighlightRows'\n";
		if(defined($row[0]) && ($row[0] ne "")) {
			if($HighlightRows ne "off") { 
				$ContinueScript = " onClick='ContinueActivity(".$row[2].")'"				
			}
			my $HtmlEncodedProject = HTML::Entities::encode($row[0]);
			my $HtmlEncodedActivity = HTML::Entities::encode($row[1]);
			$GenBody .= "<TR CLASS='highlight-$HighlightRows' ID='Project_".$row[2]."'><TD CLASS='highlight-$HighlightRows' $ContinueScript>$HtmlEncodedProject</TD><TD CLASS='highlight-$HighlightRows' $ContinueScript ID='Activity_".$row[2]."''>$HtmlEncodedActivity</TD></TR>\n";
		}
		if($HighlightRows eq "low") {
			 $HighlightRows = "high";
		} else {
			if($HighlightRows eq "high") {
				$HighlightRows = "low";
			}
		}
	}
	$GenBody .= "<TABLE>\n";
	$Response->body($GenBody);
}

sub ListProjects {
	my $Request = shift;
	my $Response = shift;	
	my $GenBody = "";
	my $SqlCmd = "SELECT DISTINCT(project) FROM timetrack";
	# No filter needed here. Filtering will be done in JS.
	#my $Filter = $Request->param('filterProjects');
	#if((defined $Filter) and ($Filter ne "")) {
	#	$SqlCmd .= " WHERE project LIKE '%$Filter%'";
	#} else {
	#	$Filter = "";
	#}
	$SqlCmd .= " ORDER BY id DESC";
	print "SqlCmd=$SqlCmd\n";
	#my $SqlCmd = "SELECT DISTINCT(project) FROM timetrack ORDER BY id DESC";
	# FF (Future Feature) get the filter from the request URI.
	#if((defined $Filter) and ($Filter ne "")) {
	#	$SqlCmd .= " WHERE project LIKE '%$Filter%'";
	#}
	# print "PopulateProjectListBox = '$SqlCmd'\n";
	$sth = $dbh->prepare($SqlCmd.";");
	$sth->execute();
	my @DefaultProjects = ("PUNCH");
	while (my @row = $sth->fetchrow_array ) {
 		#print "An entry '@row[0]'\n";
		if(defined($row[0]) && ($row[0] ne "") && ($row[0] ne "Other")) {
			#print "it is not blank\n";
			my $HtmlEncodedProject = HTML::Entities::encode($row[0]);
			if($HtmlEncodedProject ~~ @DefaultProjects) { 
				#print "Already have it in the list\n";
			} else {
				#print "Not in the list\n";
				#print "$HtmlEncodedProject\n";
				push(@DefaultProjects, $HtmlEncodedProject);			
			} 
		}
	}
	$GenBody = "<SELECT onChange='this.nextElementSibling.value=this.value'>\n<OPTION VALUE=''></OPTION>\n";
	foreach my $nP (@DefaultProjects) {		
		if(($nP ne "PUNCH") && ($nP ne "Other...")) {
			#print "Adding to project selection box '$nP'\n";
			$GenBody .= "<OPTION VALUE='$nP'>$nP</OPTION>\n";
		}
	}	
	$GenBody .= "<OPTION VALUE='Other...'>Other...</OPTION>\n<INPUT TYPE='text' ID='SelectedProject' VALUE='' onKeyPress='SearchProjects();'>\n";
	$Response->body($GenBody);
}

sub JsonProjectList {
	my $Response = shift;
	my $GenBody = "";
	my $SqlCmd = "SELECT DISTINCT(project) FROM timetrack WHERE project !='PUNCH' ORDER BY id DESC;";
	my $DpOrder = 0;
	my @DataPack;
	
	$sth = $dbh->prepare($SqlCmd);
	$sth->execute();
	while (my @row = $sth->fetchrow_array ) {
 		#print "An entry '@row[0]'\n";
		if(defined($row[0]) && ($row[0] ne ""))  {
			#print "it is not blank\n";
			# push(@DataPack, {$DpOrder++, $row[0]});
			push(@DataPack, $row[0]);
		}
	}
	my $json = encode_json \@DataPack;
	$Response->headers({ 'Content-Type' => 'application/json' });
	$Response->body($json);
}

sub Log {
	my $Request = shift;	
	print DateTime->now->set_time_zone("local")->epoch()."\t".$Request->address."\t".$Request->method."\t".$Request->request_uri."\n";
}

sub read_file {
	my $file = shift;
	my $document = do {
	    	local $/ = undef;
	    	open my $fh, "<", $file
	        or die "could not open $file: $!";
	    	<$fh>;
		};
	return $document;	
}

sub Main {
	CreateDb(); 
	my $host = "0.0.0.0";
	my $port = $dbh->selectall_arrayref("SELECT value FROM settings WHERE name = 'WebServicePort';")->[0][0];
	my $server = Twiggy::Server->new(
		host => $host,
		port => $port,
	);
	$server->register_service($app);
	print "Server running, serving on port = $port\n";
	AE::cv->recv;
} 


Main();

__DATA__
<!DOCTYPE html>
<title>People</title>
<form action="insert" method="post">
   Name: <input type="text" name="name"> 
   Age: <input type="text" name="age">
   <input type="submit" value="Add">
</form>
<br>
Data: <br>
<table border="1">
   <tr>
      <th>Name</th>
      <th>Age</th>
   </tr>
</table>