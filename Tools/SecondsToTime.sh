#!/bin/bash

function convert_to_time {
	num=$1
	min=0
	hour=0
	day=0
	printed=0
	if((num>59));then
		((sec=num%60))
		((num=num/60))
		if((num>59));then
			((min=num%60))
			((num=num/60))
			if((num>24));then
				((hour=num%24))
				((num=num/24))
				((day=num))
			else
				((hour=num))				
			fi
		else
			((min=num))
		fi
	else
		((sec=num))
	fi
	
	printf "%d,%02d:%02d:%02d" ${day} ${hour} ${min} ${sec}
}

# This will except input from a pipe or as an argument.
DATA=$1
[ "${1}x" = "x" ] && DATA=$(cat -)

convert_to_time $DATA


