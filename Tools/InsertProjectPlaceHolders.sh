#!/bin/bash

NEW_PROJECT="$1"

# Data older then three years is removed to keep the DB small and fast. That frees up IDs with lower values. Good place to inject these.
echo "INSERT INTO timetrack (id, project, activity) SELECT MIN(id)-1, '$NEW_PROJECT Admin', 'project place holder' FROM timetrack;" | tee >(sqlite3 --column --header ~/Documents/LWIDAWT.db)
sleep 5
echo "SELECT * FROM timetrack ORDER BY id LIMIT 1;"|sqlite3 -column -header ~/Documents/LWIDAWT.db
wait
echo "INSERT INTO timetrack (id, project, activity) SELECT MIN(id)-1, '$NEW_PROJECT Deployment', 'project place holder' FROM timetrack;" | tee >(sqlite3 --column --header ~/Documents/LWIDAWT.db)
sleep 5
echo "SELECT * FROM timetrack ORDER BY id LIMIT 1;"|sqlite3 -column -header ~/Documents/LWIDAWT.db
wait
echo "INSERT INTO timetrack (id, project, activity) SELECT MIN(id)-1, '$NEW_PROJECT Development', 'project place holder' FROM timetrack;" | tee >(sqlite3 --column --header ~/Documents/LWIDAWT.db)
sleep 5
echo "SELECT * FROM timetrack ORDER BY id LIMIT 1;"|sqlite3 -column -header ~/Documents/LWIDAWT.db
wait
echo "INSERT INTO timetrack (id, project, activity) SELECT MIN(id)-1, '$NEW_PROJECT Planning', 'project place holder' FROM timetrack;" | tee >(sqlite3 --column --header ~/Documents/LWIDAWT.db)
sleep 5
echo "SELECT * FROM timetrack ORDER BY id LIMIT 1;"|sqlite3 -column -header ~/Documents/LWIDAWT.db
wait
echo "INSERT INTO timetrack (id, project, activity) SELECT MIN(id)-1, '$NEW_PROJECT Support', 'project place holder' FROM timetrack;" | tee >(sqlite3 --column --header ~/Documents/LWIDAWT.db)
sleep 5
echo "SELECT * FROM timetrack ORDER BY id LIMIT 1;"|sqlite3 -column -header ~/Documents/LWIDAWT.db
wait
echo