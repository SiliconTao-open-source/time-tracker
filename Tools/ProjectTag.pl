#!/usr/bin/perl -w

use Tk;
use DBI;
use DateTime::Format::Strptime;
my $DbFile = $ENV{"HOME"}."/Documents/LWIDAWT.db";

my $mw = MainWindow->new();
#$mw->minsize( qw(615 290));
#$mw->maxsize( qw(315 90));
$mw->geometry('70x20+400+160');
$mw->title("Project Tag");
my $Sept = DateTime::Format::Strptime->new(pattern => '%B %d, %Y', on_error => 'croak');
my $SeptEpoch = $Sept->parse_datetime('September 20, 2013')->epoch();
$mw->Label(-text => "This activity was not associated with a Project.\nSelect a project from the list below or enter a new project in the box.")->pack();
my $NextPid = 0;
my $dbh;
my $frm = $mw->Frame(-borderwidth=>1, -relief => 'raised' )->pack(-padx => 10, -pady => 10, -fill => 'x'); 
my $tb = $frm->Label()->pack(-padx => 5, -pady => 5);
my $eb = $mw->Entry(-background => "white", -width => 60);

# my $lb = $mw->Listbox(-yscrollcommand => ['set', $scroll], -relief => 'sunken', -width => 60, -height => 10, -setgrid => 'yes')->pack(-padx => 5, -pady => 5);
my $lb = $mw->Listbox(-background => "white", -selectmode => "single", -relief => 'sunken', -width => 60, -height => 10, -setgrid => 'yes')->pack(-padx => 5, -pady => 5, -fill => 'both', -expand => 1);
sub SelectActivity {
	$dbh = DBI->connect("dbi:SQLite:dbname=$DbFile", "", "", {RaiseError =>1}) or die $DBI::errstr;
	my @HashRef = $dbh->selectrow_array("SELECT id,activity FROM timetrack WHERE id IN (SELECT MIN(id) FROM timetrack WHERE project IS NULL AND id > $NextPid AND time > $SeptEpoch);");
	$NextPid = $HashRef[0];
	$tb->configure(-text => $HashRef[1]);
	print "HashRef[0] = '".$HashRef[0]."' and HashRef[1] = '".$HashRef[1]."'\n";
	#$lb->pack(-side => 'left', -fill => 'both', -expand => 'yes');
	$dbh = DBI->connect("dbi:SQLite:dbname=$DbFile", "", "", {RaiseError =>1}) or die $DBI::errstr;
	my $sth = $dbh->prepare("SELECT DISTINCT(project) FROM timetrack;");
	$sth->execute();
	$lb->delete(0, 200);
	while (@row = $sth->fetchrow_array ) {
   	 # print "@row\n";
		if(defined($row[0]) && ($row[0] ne "")) {
			$lb->insert('end', @row);
		}
	}
	#$eb->configure(-text => "what");
	$eb->delete('0', 'end');
	$dbh->disconnect();
}
SelectActivity();
$eb->pack(-side => "top");
$lb->bind('<<ListboxSelect>>' => sub {
		my @temp = $lb->curselection();
		#print "selection = '".$temp[0]."'\n";
		my $Selected = $temp[0];
		$eb->configure(-text => $lb->get($Selected));
		SaveProject($NextPid, $eb->get());
		SelectActivity();
	});
my $bf = $mw->Frame()->pack(-ipadx => 20, -pady => 10);
my $execute = $bf->Button(-text => '  Update  ', -command => sub {
                           SaveProject($NextPid, $eb->get());
									SelectActivity();
                          })->pack(-side => 'left');
my $skip = $bf->Button(-text => '   Skip   ', -command => sub {
								SelectActivity();	
								})->pack(-side => 'right');

sub SaveProject {
	my $Pid = shift;
	my $Project = shift;
	$Project =~ s/^\s+//;
	$Project =~ s/\s+$//;
	if($Project ne "") {
		print "Project = '$Project'\n";
		my $dbh = DBI->connect("dbi:SQLite:dbname=$DbFile", "", "", {RaiseError =>1}) or die $DBI::errstr;
		my $sth = $dbh->prepare("UPDATE timetrack SET project=? WHERE activity=(SELECT activity FROM timetrack WHERE id=?);");
		$sth->execute($Project, $Pid);
		print "Rows updated = ".$sth->rows()."\n";
		$dbh->disconnect();
	}
}

MainLoop();
