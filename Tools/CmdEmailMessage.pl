#!/usr/bin/perl

use Mail::Message;
use Mail::Message::Body;
#use Mail::Message::TransferEnc::Base64;
#use MIME::Base64;
use File::Basename;
use Mail::Transport::SMTP;
use SiliconTao::ConfigApi;
use SiliconTao::GetInput;
use SiliconTao::PtMessage;

sub ShowHelp
{
    print "EmailFiles.pl <contents>\nContens must have:\n -s <subject>\n -b <message body> | -B <message body file>\n -t <to address>\n\nOptional values can be:\n -h (HTML formated)\n -f <file to attach>\n";
}

$HOME_DIR=$ENV{HOME};
if((! -d $HOME_DIR) || ($HOME_DIR eq ""))
{
    die("Could not find home dir");
}

if(! -d "$HOME_DIR/.email/")
{
    mkdir("$HOME_DIR/.email");
}
$MailConfig = "$HOME_DIR/.email/st_emailfiles.conf";
$SMTPServer = &ReadConf($MailConfig, "SMTP_SERVER");
if($SMTPServer eq "")
{
	&WriteConf($MailConfig, "SMTP_SERVER", "");
	$NewData = &GetInput("What is your SMTP servers address?", "mail.telusplanet.net");
	$NewData =~ s/\n//g;
	if($NewData gt "")
	{
		&WriteConf($MailConfig, "SMTP_SERVER", $NewData);
		$SMTPServer = $NewData;
	}
}

$MyAddress = &ReadConf($MailConfig, "MY_ADDRESS");
if($MyAddress eq "")
{
	&WriteConf($MailConfig, "MY_ADDRESS", "");
	$NewData = &GetInput("What is your email address?", "me\@myaddress");
	$NewData =~ s/\n//g;
	if($NewData gt "")
	{
		&WriteConf($MailConfig, "MY_ADDRESS", $NewData);
		$MyAddress = $NewData;
	}
}

if(($MyAddress eq "") || ($SMTPServer eq ""))
{
	&PtMessage("Sorry. Cannot continue without this information.", "OK");
	exit;
}

if($#ARGV > -1)
{
    $PlainText = 1;
    $HtmlText = 0;
	$MessageBody = "";
	$Subject = "No subject";
	$MessageContents = "";
	$AttachFiles = "";
	
	while ($#ARGV > -1)
	{
		$TagName = shift @ARGV;
		if($TagName eq "-t")
		{
			$SendTo = shift @ARGV;
		} 
		elsif($TagName eq "-h")
		{
			$HtmlText = 1;
			$PlainText = 0;
		}
		elsif($TagName eq "-f")
		{
			$AttachFiles = shift @ARGV;
			print "AttachFiles = '$AttachFiles'\n";
			@Attachments = split(/ /, $AttachFiles);
			for $EachAttachment (@Attachments)
			{
				if( -f $EachAttachment)
				{
					print "Attaching $EachAttachment\n";
				} else
				{
					print "Cannot find file to attach '$EachAttachment'\n";
					exit;
				}
			}
		}
		elsif($TagName eq "-s")
		{
			$Subject = shift @ARGV;
		}
		elsif($TagName eq "-B")
		{
			$MessageBodyFile = shift @ARGV;
		}
		elsif($TagName eq "-b")
		{
			$MessageBody = shift @ARGV;
		} else
		{
		    &ShowHelp();
		    exit
		}	
	}
	
	if($MessageBodyFile gt "")
	{
	    if(-f $MessageBodyFile)
	    {
		$MessageBody = `cat $MessageBodyFile`;
	    } else
	    {
		die("Cannot find that file $MessageBodyFile");
	    }
	}
	
	if($HtmlText)
	{
	    print "Sending as HTML message\n";
		$MessageType = "text/html";	
	    $MessageContents = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 TRANSITIONAL//EN\">\n";
	    $MessageContents .= "<HTML>\n<HEAD>\n<META HTTP-EQUIV=\"Content-Type\" CONTENT=\"text/html; CHARSET=UTF-8\">\n";
	    $MessageContents .= "<META NAME=\"GENERATOR\" CONTENT=\"GtkHTML/3.0.7\">\n";
	    $MessageContents .= "</HEAD>\n<BODY>\n";
	    $MessageContents .= $MessageBody;
	    $MessageContents .= "</BODY>\n</HTML>\n\n";
	} else
	{
		$MessageType = "text/plain";	
	    $MessageContents = $MessageBody;
	}
	
	if($AttachFiles gt "")
	{
		@BodyParts = ();
		for $EachAttachment (@Attachments)
		{
			$Fbn = basename($EachAttachment);
			$BodyPart = Mail::Message::Body->new(file => $EachAttachment);
			$encodedBody = $BodyPart->encode(transfer_encoding => 'base64');
			$encodedBody->disposition("attachment; filename='".$Fbn."';");
			push(@BodyParts, $encodedBody);
		}
		$DataBody = Mail::Message::Body->new(mime_type => $MessageType, data => $MessageContents);
		$MessageBody = Mail::Message::Body::Multipart->new(bountry => time.'--it-s-mime', parts => [$DataBody, @BodyParts]);
	} else
	{
	    $MessageBody = Mail::Message::Body->new(mime_type => $MessageType, data => $MessageContents);		
	}
	$NewMessage = Mail::Message->buildFromBody($MessageBody, From => $MyAddress, To => $SendTo, Subject => $Subject);
	$Connection = Mail::Transport::SMTP->new(hostname => $SMTPServer, timeout => 30);

	# This way does not seem to encode anything properly. It keeps coming back as text/plain
	#$NewMessage = Mail::Message->build(From => $MyAddress, To => $SendTo, Subject => $Subject, data => "$MessageContents", file => $AttachFile);
	#$BodyConfig = $NewMessage->body;
	#$BodyConfig->encode(mime_type => 'text/html', charset => 'utf-8', transfer_encoding => 'quoted-printable');	
	#$Connection = Mail::Transport::SMTP->new(hostname => $SMTPServer, timeout => 30);
	
	if ($Connection->send($NewMessage))
	{
		print "Message sent.\n";
	} else
	{
		print "Error sending message!\n";
		print "Erros are ".$Connection->errors() ."\n----\n";
	}
} else
{
    &ShowHelp();
    exit;
}


exit;



	if($HtmlText)
	{
	    print "Sending as HTML message\n";
		$MessageType = "text/html";	
	    $MessageContents = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 TRANSITIONAL//EN\">\n";
	    $MessageContents .= "<HTML>\n<HEAD>\n<META HTTP-EQUIV=\"Content-Type\" CONTENT=\"text/html; CHARSET=UTF-8\">\n";
	    $MessageContents .= "<META NAME=\"GENERATOR\" CONTENT=\"GtkHTML/3.0.7\">\n";
	    $MessageContents .= "</HEAD>\n<BODY>\n";
	    $MessageContents .= $MessageBody;
	    $MessageContents .= "</BODY>\n</HTML>\n\n";
	} else
	{
		$MessageType = "text/plain";	
	    $MessageContents = $MessageBody;
	}
	
	#if($AttachFile gt "")
	#{
	#	$MessageBody = Mail::Message::Body->new(mime_type => $MessageType, data => $MessageContents, file => $AttachFile);
	#} else
	#{
		$MessageBody = Mail::Message::Body->new(mime_type => $MessageType, data => $MessageContents);		
	#}
	$NewMessage = Mail::Message->buildFromBody($MessageBody, From => $MyAddress, To => $SendTo, Subject => $Subject);
	$Connection = Mail::Transport::SMTP->new(hostname => $SMTPServer, timeout => 30);
