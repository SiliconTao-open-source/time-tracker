#!/usr/bin/perl -w

use DBI;
use DateTime;
use Date::Parse;
use DateTime::Format::Strptime qw( );
use File::HomeDir;

my $DbFile = File::HomeDir->my_home."/Documents/LWIDAWT.db";
$dbh = DBI->connect("dbi:SQLite:dbname=$DbFile", "", "", {RaiseError =>1}) or die $DBI::errstr;

my $Limit = 0;
while (my $NextId2Fix = $dbh->selectall_arrayref("SELECT id FROM timetrack WHERE project='PUNCH' AND activity='Out' AND et > 1 ORDER BY time DESC LIMIT 1;")->[0][0] ) {
	printf "NextId2Fix = $NextId2Fix\n";
	my $MaxId =  $dbh->selectall_arrayref("SELECT MAX(id) FROM timetrack;")->[0][0];
	for ( my $id = $MaxId; $id > $NextId2Fix; $id--) {
		$dbh->prepare("UPDATE timetrack SET id = id+1 WHERE id=$id;\n")->execute();
	}
	my $SqlCmd = "INSERT INTO timetrack (id, project, activity, time, et) SELECT $NextId2Fix+1, project, activity, time+1, 1 FROM timetrack WHERE id=$NextId2Fix;";
	print "$SqlCmd\n";
	$dbh->prepare($SqlCmd)->execute();
	my $ET = $dbh->selectall_arrayref("SELECT et FROM timetrack WHERE id=$NextId2Fix;")->[0][0];
	$SqlCmd = "UPDATE timetrack SET project=(SELECT project FROM timetrack WHERE id=$NextId2Fix-1), activity=(SELECT activity FROM timetrack WHERE id=$NextId2Fix-1), pttw=(SELECT pttw+$ET FROM timetrack WHERE id=$NextId2Fix-1), pttm=(SELECT pttm+$ET FROM timetrack WHERE id=$NextId2Fix-1) WHERE id=$NextId2Fix;";
	print "$SqlCmd\n";
	$dbh->prepare($SqlCmd)->execute();
	if(-e "/tmp/STOP_SHIFT") {
		print "break\n";
		last;
	}
}

$dbh->disconnect();

