#!/usr/bin/perl -w


use DBI;
use DateTime;
use Date::Parse;
use DateTime::Format::Strptime qw( );
my $DbFile = $ENV{"HOME"}."/Documents/LWIDAWT.db";

sub Main {
	my $DB = 0;
	my $CreateNewDb = 0;

	# Does sqlite3 DB file exist
	if (! -e $DbFile)
	{
		# no - create a new sqlite3
		ConvertToDb();
	}

   # Scan the database for missing entries of et, dst, pttw and pttm
	# calculate elapsed time (et) and daily service time (dst)
	# need a project field to calculate project time this week (pttw) and project time this month (pttm)
	$DB = DBI->connect("dbi:SQLite:dbname=$DbFile", "", "", {RaiseError =>1}) or die $DBI::errstr;
	CalculateEt(\$DB);
	CalculateDst(\$DB);
	CalculatePttw(\$DB);
	CalculatePttm(\$DB);

	$DB->disconnect();
}

sub CalculateDst {
	my $dbh = shift;
	my $StartDate = DateTime::Format::Strptime->new(pattern => '%B %d, %Y %R %Z', on_error => 'croak')->parse_datetime('September 20, 2013 00:00 MST');
	my $EndDate = DateTime->from_object(object => $StartDate);
	$EndDate->add(days => 1);
	$EndDate->subtract(seconds => 1);
	my $Done = 10;
	while ($Done > 0) {
		my $set = $StartDate->epoch();
		my $eet = $EndDate->epoch();
		print "Day of ".$StartDate->datetime()." to ".$EndDate->datetime()."\n";
		my $Total = 0;
		my $sth = $$dbh->prepare("SELECT id, et FROM timetrack WHERE activity != 'In' AND time > $set AND time < $eet ORDER BY id;");
		$sth->execute();
		$Done--;
		while (my @data = $sth->fetchrow_array()) {
				$Done = 100;
				$Total += $data[1];
				#print "id=$data[0]\tdata[1]=$data[1]\tTotal=$Total\n";
				print " - UPDATE timetrack SET dst=$Total WHERE id=$data[0];\n";
				my $up = $$dbh->prepare("UPDATE timetrack SET dst=? WHERE id=?;");
				$up->execute($Total, $data[0]);
		}
		$StartDate->add(days => 1);
		$EndDate->add(days => 1);	
	}
}

sub CalculatePttm {
	my $dbh = shift;
	my $StartDate = DateTime::Format::Strptime->new(pattern => '%B %d, %Y %R %Z', on_error => 'croak')->parse_datetime('October 1, 2013 00:00 MST');
	#$StartDate->truncate( to => 'month');
	# Now start is the Monday after Sept 20th.
	my $EndDate = DateTime->from_object(object => $StartDate);
	$EndDate->add(months => 1);
	$EndDate->subtract(seconds => 1);
	my $Done = 10;
	while ($Done > 0) {
		my $set = $StartDate->epoch();
		my $eet = $EndDate->epoch();
		print "Month of ".$StartDate->date()." to ".$EndDate->date()."\n";
		my %Totals;
		my $sth = $$dbh->prepare("SELECT id, project, et, activity FROM timetrack WHERE et NOT NULL AND time > $set AND time < $eet ORDER BY id;");
		$sth->execute();
		my @previousData;
		$Done--;
		while (my @data = $sth->fetchrow_array()) {
				$Done = 2;
 				if(!defined($Totals{$data[1]})) {
					print "New project this month $data[1]\n";
					$Totals{$data[1]} = 0;
				}
				if($data[3] eq "Out") {
					$Totals{$previousData[1]} += $data[2];
					print " + UPDATE timetrack SET pttm=$Totals{$previousData[1]} WHERE id=$previousData[0] AND project='$previousData[1]'\tdata=$data[2]\n";
					my $up = $$dbh->prepare("UPDATE timetrack SET pttm=? WHERE id=?;");
					my $result = $up->execute($Totals{$previousData[1]}, $previousData[0]);
				} else {
					$Totals{$data[1]} += $data[2];
					print " - UPDATE timetrack SET pttm=$Totals{$data[1]} WHERE id=$data[0] AND project='$data[1]'\tdata=$data[2]\n";
					my $up = $$dbh->prepare("UPDATE timetrack SET pttm=? WHERE id=?;");
					my $result = $up->execute($Totals{$data[1]}, $data[0]);
				}
				@previousData = @data;
		}
		$StartDate->add(months => 1);
		$EndDate->add(months => 1);	
	}
}

sub CalculatePttw {
	my $dbh = shift;
	my $StartDate = DateTime::Format::Strptime->new(pattern => '%B %d, %Y %R %Z', on_error => 'croak')->parse_datetime('September 20, 2013 00:00 MST');
	$StartDate->truncate( to => 'week');
	$StartDate->add(days => 7);
	# Now start is the Monday after Sept 20th.
	my $EndDate = DateTime->from_object(object => $StartDate);
	$EndDate->add(days => 7);
	$EndDate->subtract(seconds => 1);
	my $Done = 10;
	while ($Done > 0) {
		my $set = $StartDate->epoch();
		my $eet = $EndDate->epoch();
		print "Week of ".$StartDate->date()." to ".$EndDate->date()."\n";
		my %Totals;
		my $sth = $$dbh->prepare("SELECT id, project, et, activity FROM timetrack WHERE et NOT NULL AND time > $set AND time < $eet ORDER BY id;");
		$sth->execute();
		my @previousData;
		$Done--;
		while (my @data = $sth->fetchrow_array()) {
				$Done = 10;
 				if(!defined($Totals{$data[1]})) {
					print "New project this week $data[1]\n";
					$Totals{$data[1]} = 0;
				}
				if($data[3] eq "Out") {
					$Totals{$previousData[1]} += $data[2];
					print " + UPDATE timetrack SET pttw=$Totals{$previousData[1]} WHERE id=$previousData[0] AND project='$previousData[1]'\tdata=$data[2]\n";
					my $up = $$dbh->prepare("UPDATE timetrack SET pttw=? WHERE id=?;");
					my $result = $up->execute($Totals{$previousData[1]}, $previousData[0]);
				} else {
					$Totals{$data[1]} += $data[2];
					print " - UPDATE timetrack SET pttw=$Totals{$data[1]} WHERE id=$data[0] AND project='$data[1]'\tdata=$data[2]\n";
					my $up = $$dbh->prepare("UPDATE timetrack SET pttw=? WHERE id=?;");
					my $result = $up->execute($Totals{$data[1]}, $data[0]);
				}
				@previousData = @data;
		}
		$StartDate->add(days => 7);
		$EndDate->add(days => 7);	
	}
}

sub CalculateEt {
	my $dbh = shift;
	my $LastTime = 0;
	my $NextTime = 0;
	my $StartId = 0;
	my $DeltaTime = 0;
	my $Sept = DateTime::Format::Strptime->new(pattern => '%B %d, %Y %Z', on_error => 'croak');
	my $SeptEpoch = $Sept->parse_datetime('September 20, 2013 MST')->epoch();
	my @HashRef = $$dbh->selectrow_array("SELECT MIN(id) FROM timetrack WHERE time > $SeptEpoch AND et IS NULL;");
	if(defined($HashRef[0])) {
		$StartId = $HashRef[0];
		if($HashRef[0] > 1)
		{
			@HashRef = $$dbh->selectrow_array("SELECT time FROM timetrack WHERE id=$StartId - 1;");
			$LastTime = $HashRef[0];
		}
		@HashRef = $$dbh->selectrow_array("SELECT time FROM timetrack WHERE id=$StartId;");
		$NextTime = $HashRef[0];
		while($NextTime ne "") {
			if($LastTime > 0) {
				#print "LastTime = '$LastTime'\n";
				#print "NextTime = '$NextTime'\n";
				$DeltaTime = $NextTime - $LastTime;
				if ($DeltaTime > 7200) {
					#print "Missed puch out, adjust to 2 hours. ID=$StartId\n";
					$DeltaTime = 7200;
				}
				#print "UPDATE timetrack SET et=$DeltaTime WHERE id=$StartId;\n";
				$$dbh->prepare("UPDATE timetrack SET et=$DeltaTime WHERE id=$StartId;")->execute();
			}
			$StartId++;
			$LastTime = $NextTime;
			$NextTime = "";
			@HashRef = $$dbh->selectrow_array("SELECT time FROM timetrack WHERE id=$StartId;");
			if(defined($HashRef[0])) {
				$NextTime = $HashRef[0];
			}
		}
	}
}

sub ConvertToDb {
	my $NextId = 0;
	my $Activity;
	#my $DateFormat = DateTime::Format::Strptime->new(pattern   => '%a %b %e %H:%M:%S %Y', time_zone => 'Canada/East-Saskatchewan', on_error  => 'croak');
	$DB = DBI->connect("dbi:SQLite:dbname=$DbFile", "", "", {RaiseError =>1}) or die $DBI::errstr;
	$DB->do("CREATE TABLE timetrack (id INT, project CHAR(30), activity CHAR(500), time INT, et INT, dst INT, pttw INT, pttm INT);");
	my $Date = DateTime->now->set_time_zone('local');
	#my $MyEpoch = $Date->epoch();
	my $MyEpoch = 10;

	my @DefaultProjects = ("PUNCH");
	foreach (@DefaultProjects) {
		$SqlCmd = "INSERT INTO timetrack (id, project, time) VALUES (?, ?, ?)";
		$dbh->prepare($SqlCmd)->execute($NextId, $_, $MyEpoch);
		$NextId++;
	}
	$SqlCmd = "INSERT INTO timetrack (id, project, activity, time) VALUES (?, ?, ?, ?)";
	$dbh->prepare($SqlCmd)->execute($NextId, "PUNCH", "Out", $MyEpoch+10);

	# Populate it with data from the old file in docuements
	open FILEHANDLE, "tac \"".$ENV{"HOME"}."/Documents/Look\ what\ I\ did\ at\ work\ today.txt\" |";
	while(<FILEHANDLE>) {
		#print "Line = $_\n";
		if( $_ =~ m/(?<activity>.*) ; (?<date>.*)/) {
			$Activity = $+{activity}; 
			$Date = $+{date}; 
			$Activity =~ s/^\s+//;
			$Activity =~ s/\s+$//;
			print "activity = '$Activity'\t";
			print "date='$Date'\n";
			$Date =~ s/^\s+//;
			$Date =~ s/\s+$//;
			$MyEpoch = str2time($Date);
			$SqlCmd = "INSERT INTO timetrack (id, activity, time) VALUES (?, ?, ?)";
			#print "$SqlCmd\n";
			#print "MyEpoch = '$MyEpoch'\n";
			$DB->prepare($SqlCmd)->execute($NextId, $Activity, $MyEpoch);
			$NextId++;
		}		
	}
	close FILEHANDLE;
	
	$DB->disconnect();
}

Main();
