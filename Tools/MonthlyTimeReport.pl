#!/usr/bin/perl -w

use DBI;
use DateTime;
use Date::Parse;
use DateTime::Format::Strptime qw( );
use File::HomeDir;

my $DbFile = File::HomeDir->my_home."/Documents/LWIDAWT.db";
#my $StartDate = DateTime::Format::Strptime->new(time_zone => 'America/Los_Angeles', pattern => '%F %R', on_error => 'croak')->parse_datetime($ARGV[0].' 00:00');
my $StartDate = DateTime::Format::Strptime->new(pattern => '%F %R', on_error => 'croak')->parse_datetime($ARGV[0].' 00:00');
#$StartDate->configure(time_zone => 'America/Los_Angeles');
$StartDate->truncate( to => 'month');
my $EndDate = DateTime->from_object(object => $StartDate);
$EndDate->add(months => 1);
$EndDate->subtract(seconds => 1);
my $set = $StartDate->epoch();
my $eet = $EndDate->epoch();
$dbh = DBI->connect("dbi:SQLite:dbname=$DbFile", "", "", {RaiseError =>1}) or die $DBI::errstr;
print "Monthly Activity Report from ".$StartDate->day_name()." ".$StartDate->month_name()." ".$StartDate->day().", ".$StartDate->year()." to ".$EndDate->day_name()." ".$EndDate->month_name()." ".$EndDate->day().", ".$EndDate->year()."\n";
print "-------------------------------------------------------\n";
my $TotalSeconds = 0;
my $SqlCmd = "SELECT DISTINCT(project),MAX(pttm) FROM timetrack WHERE project != 'PUNCH' AND time > $set AND time < $eet GROUP BY project ORDER BY pttm DESC;";
#print "$SqlCmd\n";
my $sth = $dbh->prepare($SqlCmd);
$sth->execute();
printf "%-50s | %-20s\n", 'Project', 'Hours:Minutes';

while (my @data = $sth->fetchrow_array()) {
	$TotalSeconds += $data[1];
	my $Hours = int ($data[1] / 60 / 60);
	my $Minutes = int (($data[1] - ($Hours * 60 * 60)) / 60);
	my $Seconds = ($data[1] - ($Hours * 60 * 60) - ($Minutes * 60));
	printf "%-50s | %01d:%02d:%02d\n", $data[0], $Hours, $Minutes, $Seconds;
}

my $Hours = int ($TotalSeconds / 60 / 60);
my $Minutes = int (($TotalSeconds - ($Hours * 60 * 60)) / 60);
my $Seconds = ($TotalSeconds - ($Hours * 60 * 60) - ($Minutes * 60));
printf "\n\nTotal hours:minutes this month = %01d:%02d:%02d\n", $Hours, $Minutes, $Seconds;

$dbh->disconnect();

