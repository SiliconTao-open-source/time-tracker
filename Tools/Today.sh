#!/bin/bash

echo -e ".width 8 20 50 30\nSELECT id,project,activity,strftime('%Y-%m-%d %H:%M:%S',datetime(time-$((60*60*6)),'unixepoch')) AS time,et, dst, pttw, pttm FROM timetrack WHERE time > $(date -d"00:00:00" +%s) ORDER BY time;"|sqlite3  --column --header ~/Documents/LWIDAWT.db