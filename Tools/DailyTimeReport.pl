#!/usr/bin/perl -w

use DBI;
use DateTime;
use Date::Parse;
use DateTime::Format::Strptime qw( );
use Config;
use File::HomeDir;

my $DbFile = File::HomeDir->my_home."/Documents/LWIDAWT.db";
my $StartDate = DateTime::Format::Strptime->new(time_zone => 'local', pattern => '%F %R', on_error => 'croak')->parse_datetime($ARGV[0].' 00:00');
my $EndDate = DateTime->from_object(object => $StartDate);
$EndDate->add(days => 1);
$EndDate->subtract(seconds => 1);
my $set = $StartDate->epoch();
my $eet = $EndDate->epoch();
$dbh = DBI->connect("dbi:SQLite:dbname=$DbFile", "", "", {RaiseError =>1}) or die $DBI::errstr;
my $Seconds = $dbh->selectall_arrayref("SELECT MAX(dst) FROM timetrack WHERE time > $set AND time < $eet;")->[0][0];
if(defined($Seconds)) {
	my $ConvertTime = DateTime::Format::Strptime->new(pattern => '%s', on_error => 'croak')->parse_datetime($Seconds);
	print "Daily Activity Report for ".$StartDate->day_name()." ".$StartDate->month_name()." ".$StartDate->day().", ".$StartDate->year()."\n";
	print "Total Daily Service Time = ".$ConvertTime->hour().":".$ConvertTime->minute()."\n";
	print "-------------------------------------------------------\n";

	my $sth = $dbh->prepare("SELECT id, project, activity, time FROM timetrack WHERE time > $set AND time < $eet ORDER BY id DESC;");
	$sth->execute();
	printf "%-20s | %-30s | %-100s\n", 'Time stamp', 'Project', 'Activity';

	while (my @data = $sth->fetchrow_array()) {
		my $ConvertDate = DateTime::Format::Strptime->new(time_zone => 'America/Edmonton', pattern => '%s', on_error => 'croak')->parse_datetime($data[3]);
		printf "%-20s | %-30s | %-100s\n", $ConvertDate->date()." ".$ConvertDate->time(), $data[1], $data[2];
	}	
} else {
	print "No data to report for ".$StartDate->day_name()." ".$StartDate->month_name()." ".$StartDate->day().", ".$StartDate->year()."\n";
}



$dbh->disconnect();

