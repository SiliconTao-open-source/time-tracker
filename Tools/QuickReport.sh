#!/bin/bash

DEBUG=0
[ "${1}" = "-d" ] && {
	DEBUG=1
	shift
}

GREP_COMMAND="cat -"
[ ${#@} -gt 0 ] && {
	GREP_COMMAND="grep -iE \"$1"
	shift
	while [ ${#@} -gt 0 ] ; do
		GREP_COMMAND="$GREP_COMMAND|$1"
		shift
	done
	GREP_COMMAND="$GREP_COMMAND\""
}
#echo "GREP_COMMAND='$GREP_COMMAND'"
#exit

APP_NAME=$(basename $0)
SELECT_SIZE=$(( $(tput lines) - 3 ))
[ ${APP_NAME//Detail/} != $APP_NAME ] && {
	SELECT_SIZE=500
}
[ ${APP_NAME//Long/} != $APP_NAME ] && {
	SELECT_SIZE=500000
}
TOTAL_COLS=$(tput cols)
PROJECT_COL=$(( (TOTAL_COLS - 22) / 3 ))
ACTIVITY_COL=$(( TOTAL_COLS - 22 - PROJECT_COL ))
SQL_CMD=".width 20 $PROJECT_COL $ACTIVITY_COL\nSELECT strftime('%Y-%m-%d %H:%M:%S',datetime(time-25200,'unixepoch')) AS time,project,activity FROM timetrack ORDER BY time DESC LIMIT $SELECT_SIZE;"
[ ${APP_NAME//Detail/} != $APP_NAME ] && {
	SQL_CMD=".width 19 5 30 100\n SELECT strftime('%Y-%m-%d %H:%M:%S',datetime(time-25200,'unixepoch')) AS time, * FROM timetrack ORDER BY time DESC LIMIT $SELECT_SIZE;"
}
[ $DEBUG -eq 1 ] && { 
	echo "echo -e \"$SQL_CMD\"|sqlite3 --column --header ~/Documents/LWIDAWT.db"
	exit
}
# echo "echo -e \"$SQL_CMD\"|sqlite3 --column --header ~/Documents/LWIDAWT.db | sed -e 's/ *$//g' | ( $GREP_COMMAND )"
eval "echo -e \"$SQL_CMD\"|sqlite3 --column --header ~/Documents/LWIDAWT.db | sed -e 's/ *$//g' | $GREP_COMMAND "

# echo -e ".width 15 50 6 6 60\n SELECT strftime('%Y-%m-%d %H:%M:%S',datetime(time-25200,'unixepoch')) AS time,project,* FROM timetrack ORDER BY time DESC LIMIT $(( $(tput lines) - 10));" | sqlite3 -column -header ~/Documents/LWIDAWT.db
