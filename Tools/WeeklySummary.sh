#!/bin/bash

# This script is only for Royce. It generates a time report that is used for an invoice.

# Create a time report for last week.
# ./WeeklyTimeReport.pl $(date +%F -d"1 week ago") >> ~/Documents/Invoices/2014/2014-02-03/TimeTrack.txt
# Build a directory for every second week
# MD="2014-01-20"; for i in $(seq 22); do MD=$(date +%F -d"$MD 2 weeks"); echo mkdir -v $MD; done

BASE_DIR=$HOME/Documents/Invoices/
[ -f ${HOME}/.time-tracker ] && source ${HOME}/.time-tracker

APP_PATH=$(dirname $0)
# echo $APP_PATH/WeeklyTimeReport.pl
FINAL_REPORT=0
REPORT_DIR=
USE_DATE=$(date +%F -d"this monday")
TODAY=$(date +%u)
[ ! "$1" = "" ] && {
	echo "Using set date $1"
	USE_DATE=$1
} 

TEST_DIR=$BASE_DIR/$(date +%Y)/$USE_DATE
echo "Look for TEST_DIR=$TEST_DIR"
[ -d $TEST_DIR ] && {
	echo "Final week of invoice."
	FINAL_REPORT=1
	REPORT_DIR=$TEST_DIR
} || { 
	echo "Did not find it."
	TEST_DIR=$BASE_DIR/$(date +%Y -d"$USE_DATE 1 week")/$(date +%F -d"$USE_DATE 1 week")
	echo "Look for TEST_DIR=$TEST_DIR"
	[ -d $TEST_DIR ] && {
		echo "First report of this invoice."
		REPORT_DIR=$TEST_DIR
	} || { 
		echo "Did not find that one either!"
	}
}
[ -L "${BASE_DIR}/Current_week" ] && rm -vf "${BASE_DIR}/Current_week"
ln -vs "$REPORT_DIR" "${BASE_DIR}/Current_week" 
echo "USE_DATE=$USE_DATE"
echo "REPORT_DIR=$REPORT_DIR"

[ -z "$REPORT_DIR" ] || {
	echo "Using REPORT_DIR=$REPORT_DIR"
	LAST_MONDAY=$(date +%F -d"$USE_DATE 1 week ago")
	echo $APP_PATH/WeeklyTimeReport.pl $LAST_MONDAY
	$APP_PATH/WeeklyTimeReport.pl $(date +%F -d"$USE_DATE 1 week ago") >> $REPORT_DIR/TimeTrack.txt
	$APP_PATH/InvoiceDetails.sh >> $REPORT_DIR/Details.txt
	echo "SELECT DISTINCT(activity) FROM timetrack WHERE time > $(date +%s -d"7 day ago 00:00:00") AND project != 'PUNCH';"|sqlite3 ~/Documents/LWIDAWT.db|sort -u > $REPORT_DIR/Weekly_Accomplishments_$(date +%F).txt
	[ $FINAL_REPORT -eq 1 ] && {
		echo "Final report"
		cp ${BASE_DIR}/Invoice_template.ods ${TEST_DIR}/Invoice_${USE_DATE}.ods
		cp ${BASE_DIR}/TES_statement.ods ${TEST_DIR}/TES_statement_${USE_DATE}.ods
		cat $REPORT_DIR/TimeTrack.txt | mutt -s "Invoice due" royce@sample.fake
	}

	for NEXT_DAY in $(seq 0 6); do 
		REPORT_DAY=$(date +%F -d"$LAST_MONDAY $NEXT_DAY days")
		echo -e "\n"
		echo "TES weekly report for $REPORT_DAY"
		$APP_PATH/TES_Report.sh $REPORT_DAY 2>&1
	done >> $REPORT_DIR/TES_weekly_report_$(date +%F -d"$LAST_MONDAY 7 days").txt
} 

[ -z "$REPORT_DIR" ] && {
	echo "The value of REPORT_DIR is empty."
	echo "Cannot find a report directory for invoice summary." | mutt -s "Invoice problem" royce@sample.fake
}

