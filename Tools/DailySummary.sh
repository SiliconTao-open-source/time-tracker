#!/bin/bash

BASE_DIR=$HOME/Documents/Invoices/
[ -f ${HOME}/.time-tracker ] && source ${HOME}/.time-tracker

TD=yesterday
[ "${1}x" != "x" ] && {
	TD=$1
}

# Current_week is a symlink that cron adds to point to the current week of work
DIR=${BASE_DIR}/Current_week
ST=$(date +%s -d"$TD 00:00:01")
ET=$(( ST + 86400 ))

mkdir -p ${DIR}/DailySummary/
OUT=$(date +%F -d"$TD").txt

TOTAL_DAY=0
SQL="SELECT DISTINCT(project), MAX(pttd) AS time FROM timetrack WHERE project != 'PUNCH' AND time > ${ST} AND time < ${ET} GROUP BY project ORDER BY dst DESC;"
echo "$SQL" >&2
WIDTH=10
WIDTH=$(echo "$SQL" | sqlite3 ~/Documents/LWIDAWT.db | tr "|" " " | while read LINE; do
	SECONDS=$(echo $LINE | awk '{print $NF}')
	NL=$(echo $LINE | sed -e "s/$SECONDS$//")
	[ ${#NL} -gt $WIDTH ] && {
		WIDTH=${#NL}
		echo $WIDTH
	}
done | tail -n 1)

echo "DailySummary for $(date +%F -d"@$ST")" > ${DIR}/DailySummary/$OUT
printf "%-${WIDTH}s %s\n" "Project" "Time" >> ${DIR}/DailySummary/$OUT
eval $(echo "printf "=%.0s" {1..${WIDTH}}") >> ${DIR}/DailySummary/$OUT
echo ===== >> ${DIR}/DailySummary/$OUT

echo "$SQL" | sqlite3 ~/Documents/LWIDAWT.db | tr "|" " " | while read LINE; do
	echo $LINE >&2
	DSECONDS=$(echo $LINE | awk '{print $NF}')

	# Add 7:30 minutes to round the 15 minues
	RSECONDS=$(bc <<< "$DSECONDS + 750")

	# Quarter hours as fraction of hours 
	printf "%-${WIDTH}s %d.%02d\n" "$(echo "$LINE"|sed -e "s/${DSECONDS}$//")" $(bc <<< "scale=0; a=$RSECONDS/60/60; b=($RSECONDS/60)%60; a/1; scale=2; f=((b / 15)); scale=0; s=f/1; s * 25")
done >> ${DIR}/DailySummary/$OUT
TOTAL=$(bc <<< "scale=2; $(echo $(cat ${DIR}/DailySummary/$OUT | sed -e '0,/======/d'|awk '{print $NF,"+"}')) 0")

echo ---------------------------------------- >> ${DIR}/DailySummary/$OUT 
echo "Total hours = $TOTAL" >> ${DIR}/DailySummary/$OUT
cat ${DIR}/DailySummary/$OUT >&2 
