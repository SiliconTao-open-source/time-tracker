#!/usr/bin/env perl -w


# =============================================================================
#
#    Copyright (C) 2019 Silicon Tao Technology Systems Inc.
#    E-mail:  support@SiliconTao.com
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#=============================================================================

# Tk referances 
# ibm.com/developerworks/library/l-ptkwidg/
# docstore.mik.ua/orelly/perl3/tk/ch05_02.htm

use warnings;
use strict;
use Tk;
use Tk::DialogBox;
use Tk::Labelframe;
use Tk::LabEntry;
use Tk::BrowseEntry;
use Tk::NoteBook;
use DateTime;
use DateTime::Format::Strptime qw( );
#use DBI;
use Fcntl qw(:flock);
#use Mail::Message;
#use Mail::Message::Body;
#use Mail::Reporter;
#use Mail::Transport::SMTP;
#use Net::SMTP;
use Sys::Hostname;
use POSIX;
use FindBin qw($RealBin);
use lib "$RealBin";
use TimeTracker;
use File::Basename;
#use Cwd;

my $mw;	# Main window
my $InfoFrame; 
my $iofm; 
my $ioHelp;
my $ib; 
my $ob;
my $id;
my $idEntry;
my $TimeFrame;
my $TodayTime;
my $WeekTime;
my $na;
my $NewHelp;
my $be, 
my $eb;
my $pb;
my $frm;
my $sw;  	# Settings window
my $dre;	# daily report emails
my $dreeb;  # daily report emails edit box
my $swsb;	# settings window save button
my $sf;		# settings frame
my $sb;		# settings button
my $TextBox;
my $book;
my $ToolsTab;
my $mea;
my $wre;
my $mreeb;
my $mre;
my $meaeb;
my $mms;
my $SettingsTab;
my $mmseb;
my $ReportsTab;
my $WebTab;
my $WtPortEdit;
my $WtLoginNameEdit;
my $WtLoginPassLf;
my $WtLoginPassEdit;
my $WtPeLf;
my $WtLoginLf;
my $WtLoginNameLf;
my $WtViewLf;
my $WtViewNameLf;
my $WtViewNameEdit;
my $WtViewPassLf;
my $WtViewPassEdit;
my $WtSaveBtn;
my $WtServiceLf;
my $WtCtlLf;
my $ButtonBar;
my $ttf;
my $wreeb;
my $RecentHelp;
my $rlb;
my $LastTyping;
my $TimeOutTyping;
my $AutoCron = 0;
my $Debug = 0;
my $AppPath = dirname(__FILE__);

# my $MyTimeZone = "Canada/Saskatchewan";

# This works in Linux
#print "hostname = ".hostname()."\n";
#exit;
# This works in Linux

Main();

sub Main {
	CreateDb(); 
	while(scalar @ARGV) {
		my $A1 = shift @ARGV;
		
		if($A1 eq "-a") {
			# Only allow cron to run this if being run from the same host as last time.
			if( ! ImLastHostIn() ) {
				print "This system was not the last host to be used for PUNCH IN.\n";
				exit 78;
			}
			print "This system was the last host to be used for PUNCH IN. Continue with calculations.\n";		
			$AutoCron = 1;
		}
		if($A1 eq "-d") {
			$Debug = 1;
		}
	}

	OnlyOne();
	if($AutoCron == 1) {
		CalculateLastActivityTime();
	}

	# if (defined($ARGV[0]) and ($ARGV[0] eq "-a") ) {	
	BuildGui();
	DisplayHoursWorked();
	PopulateProjectListBox();
	PopulateActivityHistory();
	$LastTyping = "";
	CheckPunchStatus();
	StartATimer();	
	MainLoop();
	Destructor();
}

sub StartATimer {
	#print "StartATimer\n";	
	$TimeOutTyping = 0;
	$mw->after(1000, sub { Tick(); } );
}

sub Tick {
	$TimeOutTyping++;

	# The bind modified does not seem to be working so I am going to compare strings
	my $NewTyping = $eb->get();
	if ( $NewTyping ne $LastTyping ) {
		$TimeOutTyping = -100;
		$LastTyping = $NewTyping;
		
		if(length($NewTyping) > 2) {
			FilterActivityList();
		}
	}
	
	print "Time = $TimeOutTyping\n";
	if($TimeOutTyping > 50) {
		#print "Time out\n";
		IncFailedToEnterCount();
		Destructor();
	}
	$mw->after(1000, sub { Tick(); } );
}

sub ImLastHostIn {
	my $LastHostIn = $dbh->selectall_arrayref("SELECT value FROM settings WHERE name = 'LastHostIn';")->[0][0];
	if ( $LastHostIn eq hostname() ) {
		return 1;
	} else {
		return 0;
	}
}

sub IncFailedToEnterCount {
	if($Debug == 1)	 {
		printf "IncFailedToEnterCount\n";
	}
	my $Counter = $dbh->selectall_arrayref("SELECT value FROM settings WHERE name = 'FailedToEnterCounter';")->[0][0];
	# $dbh->do("CREATE TABLE settings (name CHAR(50), value CHAR(500));");
	if(! defined($Counter)) {
		$dbh->prepare("INSERT INTO settings (name, value) VALUES ('FailedToEnterCounter', '0');")->execute;
		$Counter = 0;
	}
	$Counter++;
	if($Counter < 15) {
		$dbh->prepare("UPDATE settings SET value='$Counter' WHERE name = 'FailedToEnterCounter';")->execute;	
	} else {
		SaveInOut('Out');
	}
}

sub ActivityTyping {
	print "type, type, type. Get a real job!\n";
	$TimeOutTyping = 0;
}

sub PopulateProjectListBox {
	my $Filter = shift;
	my $SqlCmd = "SELECT DISTINCT(project) FROM timetrack";

	if((defined $Filter) and ($Filter ne "")) {
		$SqlCmd .= " WHERE project LIKE '%$Filter%'";
	}
	$SqlCmd .= " ORDER BY id DESC";
	print "PopulateProjectListBox = '$SqlCmd'\n";
	$sth = $dbh->prepare($SqlCmd.";");
	$sth->execute();
	$be->Subwidget("slistbox")->delete(0, 'end');
	my @DefaultProjects = ("PUNCH");
	while (my @row = $sth->fetchrow_array ) {
 		#print "An entry '@row[0]'\n";
		if(defined($row[0]) && ($row[0] ne "") && ($row[0] ne "Other")) {
			#print "it is not blank\n";
			if($row[0] ~~ @DefaultProjects) { 
				#print "Already have it in the list\n";
			} else {
				#print "Not in the list\n";
				push(@DefaultProjects, $row[0]);			
			} 
			#foreach my $OneOff (@DefaultProjects) {
			#	print "oneoff = '$OneOff'\n";
			#}
		}
	}
	# Use sort for alphabetical projects, remove it for most recent.
	# foreach my $nP (sort @DefaultProjects) {		
	foreach my $nP (@DefaultProjects) {		
		if(($nP ne "PUNCH") && ($nP ne "Other...")) {
			#print "Adding to project selection box '$nP'\n";
			$be->insert('end', $nP);
		}
	}	
	$be->insert('end', "Other...");
}

sub PopulateActivityHistory {
	print "PopulateActivityHistory\n";
	#$sth = $dbh->prepare("SELECT DISTINCT(activity) FROM timetrack ORDER BY time DESC LIMIT 150;");
	#$sth = $dbh->prepare("SELECT project,activity FROM timetrack WHERE activity IS NOT 'In' AND activity IS NOT 'Out' AND activity IS NOT NULL GROUP BY project,activity ORDER BY id DESC LIMIT 50;");
	# By using MAX(id) we can see the results in order of time because GROUP will try to use the first match MIN(id)
	$sth = $dbh->prepare("SELECT project,activity FROM timetrack WHERE project IS NOT 'PUNCH' AND activity IS NOT NULL GROUP BY project,activity ORDER BY MAX(id) DESC LIMIT 50;");
	$sth->execute();
	while (my @row = $sth->fetchrow_array ) {
  		 # print "@row\n";
		if($row[0] ne "") {
			# $rlb->insert('end', @row);
			$rlb->insert('end', $row[0]."  |".chr($BlankChar).$row[1]);
		}
	}
}

sub FilterProjectMenu {
	# print "Filter project\n";
	my $FiletString = $be->Subwidget('entry')->get;
	if(length($FiletString) > 2) {
		# print "FiletString = $FiletString\n";
		# $be->Subwidget('entry')->delete(0, 'end');
		PopulateProjectListBox($FiletString);
	}
}

sub FilterActivityList {
	$rlb->delete(0, 'end');
	my $NewText = $eb->get();
	$sth = $dbh->prepare("SELECT project,activity FROM timetrack WHERE activity IS NOT 'In' AND activity IS NOT 'Out' AND activity LIKE '%$NewText%' GROUP BY project,activity ORDER BY id DESC LIMIT 50;");
	$sth->execute();
	while (my @row = $sth->fetchrow_array ) {
  		 # print "@row\n";
		if($row[0] ne "") {
			$rlb->insert('end', $row[0]."  |".chr($BlankChar).$row[1]);
		}
	}	
}

sub BuildGui {
	# $mw->Label(-text => "Options:\n1 - select a project and enter a New activity description to record\n2 - record in or out activity\n3 - select a recent activity to record on going work")->pack();
	$mw = MainWindow->new;
	$mw->geometry('92x20+400+160');
	my $TitleString = "Record Your Current Work Activity";
	if($Debug == 1) { 
		$TitleString = "Running in debug mode.";
	}
	$mw->title($TitleString);

	$InfoFrame = $mw->Labelframe(-borderwidth=>0)->pack(-padx => 5, -pady => 5);
	if($Debug == 1) {
		$InfoFrame->configure(-background=>"red");
	}
	$iofm = $InfoFrame->Labelframe( -borderwidth => 1, -relief => 'raised', -text => "In and Out" )->pack(-padx => 5, -pady => 5, -side => 'left');
	$ioHelp = $iofm->Label(-text => "in or out")->pack(-padx => 20);
	$ib = $iofm->Button(-text => ' In ', -command => sub {
		SaveInOut('In');
		Destructor();
	}, -width=>10)->pack(-side => 'left', -padx => 15, -pady => 5, -expand=>'x');

	$ob = $iofm->Button(-text => ' Out ', -command => sub {
		SaveInOut('Out');
		Destructor();
	}, -width=>10)->pack(-side => 'right', -padx => 15, -pady => 5, -expand=>'x');
	

	$TimeFrame = $InfoFrame->Labelframe( -borderwidth => 1, -relief => 'raised', -text => "Times", -width =>100, -height => 10)->pack(-padx => 3, -pady => 3, -side => 'left');
	$TodayTime = $TimeFrame->Label(-text => "Today: 3:56", -width =>40, -height => 2)->pack(-side => 'top');
	$WeekTime = $TimeFrame->Label(-text => "This week: 43:18")->pack(-side => 'top');
	$sb = $InfoFrame->Button(-text=>'Settings & Tools', -command=>sub{Settings();}, -height=>3, -width=>20)->pack(-side => 'right', -padx => 15, -pady => 5);

	$na = $mw->Labelframe(-borderwidth=>1, -relief => 'raised', -text => "New activity", -width => 90 , -height => 15)->pack(-fill => 'x');
	$NewHelp = $na->Label(-text => "Select a project and enter an activity description then click record. Or type a few letters to filter recent activity.")->pack(-padx => 5);
	$be = $na->BrowseEntry(-background => "white", -label => "Project")->pack(-side => 'left',-padx => 5);
	$eb = $na->Entry(-background => "white")->pack(-side => "left", -fill => 'x', -expand => 1,-padx => 5);
	$pb = $na->Button(-text => 'Record', -command => sub {
		SaveActivity();
		Destructor();
	})->pack(-side => 'right',-padx => 5);
	$eb->bind('<KeyRelease-Return>' => sub {
		SaveActivity();
		Destructor();
	});
	# $eb->bind('<<Modified>>' => sub { ActivityTyping });
	$be->bind('<KeyRelease>' => sub { FilterProjectMenu });
 
	$frm = $mw->Labelframe(-borderwidth=>1, -relief => 'raised', -text => "Recent activity" )->pack(-fill => 'both', -expand => 1);
	$RecentHelp = $frm->Label(-text => "Click once on a recent activity description to record on going work")->pack();
	$rlb = $frm->Listbox(-background => "white", -selectmode => "single", -relief => 'sunken', -width => 90, -height => 15, -setgrid => 'yes')->pack(-padx => 5, -pady => 5, -fill => 'both', -expand => 1);

	$rlb->bind('<<ListboxSelect>>' => sub {
		my @temp = $rlb->curselection();
		my $Selected = $temp[0];
		print "selected index = '$Selected'\n";
		ContineWork($rlb->get($Selected));
		Destructor();
	});
	
	$id = $mw->DialogBox(-title => 'Input box', -buttons => ['Ok', 'Cancel'], -default_button => 'Ok');
	#$id->add('LabEntry', -width => 20, -label => 'New text', -labelPack => [-side => 'left'], -pady=>5, -padx=>5)->pack;
		
	$idEntry = $id->Entry(-background => "white")->pack(-side => "left", -fill => 'x', -expand => 1, -padx => 5, -pady=> 5);

	# $id->add('LabEntry', -width => 20, -label => 'New text', -labelPack => [-side => 'left'])->pack;
}

sub Settings {	
	$TimeOutTyping = -100;
	$sw = $mw->Toplevel(-title=>"Settings", -height=>300, -width=>400);
	$book = $sw->NoteBook->pack(-fill=>'both', -expand=>1);
	
	$ReportsTab = $book->add( "ReportsTab", -label=>"Reports"); # , -createcmd=>\&ShowReportsTab );
	$SettingsTab = $book->add( "SettingsTab", -label=>"Settings"); # -raisecmd=>\&ShowSettingsTab );
	$ToolsTab = $book->add( "ToolsTab", -label=>"Tools"); # , -raisecmd=>\&ShowToolsTab );
	$WebTab = $book->add( "WebTab", -label=>"Web");

	# $sw->geometry('92x20+400+160');
	my $TitleString = "Settings";
	if($Debug == 1) { 
		$TitleString = "Running in debug mode.";
	}
	$sw->title($TitleString);
	$mms = $SettingsTab->Labelframe(-borderwidth=>1, -relief => 'raised', -text => "Use this email server to send email", -width => 190 , -height => 15)->pack(-side=>'top', -fill => 'both', -expand => 1, -padx=>10, -pady=>20);
	$mmseb = $mms->Entry(-background => "white", -width=>70)->pack(-side => "left", -fill => 'x', -expand => 1,-padx => 10, -pady=>10);

	$mea = $SettingsTab->Labelframe(-borderwidth=>1, -relief => 'raised', -text => "My email address", -width => 190 , -height => 15)->pack(-side=>'top', -fill => 'both', -expand => 1, -padx=>10, -pady=>20);
	$meaeb = $mea->Entry(-background => "white", -width=>70)->pack(-side => "left", -fill => 'x', -expand => 1,-padx => 10, -pady=>10);

	$dre = $SettingsTab->Labelframe(-borderwidth=>1, -relief => 'raised', -text => "Send daily report emails to these addresses (seperated by a space)", -width => 190 , -height => 15)->pack(-side=>'top', -fill => 'both', -expand => 1, -padx=>10, -pady=>20);
	$dreeb = $dre->Entry(-background => "white", -width=>70)->pack(-side => "left", -fill => 'x', -expand => 1,-padx => 10, -pady=>10);

	$wre = $SettingsTab->Labelframe(-borderwidth=>1, -relief => 'raised', -text => "Send weekly report emails to these addresses (seperated by a space)", -width => 190 , -height => 15)->pack(-side=>'top', -fill => 'both', -expand => 1, -padx=>10, -pady=>20);
	$wreeb = $wre->Entry(-background => "white")->pack(-side => "left", -fill => 'x', -expand => 1,-padx => 10, -pady=>10);

	$mre = $SettingsTab->Labelframe(-borderwidth=>1, -relief => 'raised', -text => "Send monthly report emails to these addresses (seperated by a space)", -width => 190 , -height => 15)->pack(-side=>'top', -fill => 'both', -expand => 1, -padx=>10, -pady=>20);
	$mreeb = $mre->Entry(-background => "white")->pack(-side => "left", -fill => 'x', -expand => 1,-padx => 10, -pady=>10);

	$swsb = $SettingsTab->Button(-text => 'Save', -command => sub { SaveSettings(); })->pack(-side => 'right',-padx => 5, -pady=>15);

	$WtServiceLf = $WebTab->Labelframe(-borderwidth=>1, -relief => 'raised', -text => "Web service", -width => 190 , -height => 15)->pack(-side=>'top', -fill => 'both', -expand => 1, -padx=>10, -pady=>20);
	$WtPeLf = $WtServiceLf->Labelframe(-borderwidth=>1, -relief => 'raised', -text => "Port number", -width => 150 , -height => 15)->pack(-side=>'left', -expand => 0, -padx=>10, -pady=>20);
	$WtPortEdit = $WtPeLf->Entry(-background => "white", -width=>10)->pack(-side => "left", -expand => 0, -padx=>10, -pady=>10);

	$WtCtlLf = $WtServiceLf->Labelframe(-borderwidth=>1, -relief => 'raised', -text => "Service", -width => 150 , -height => 15)->pack(-side=>'right', -expand => 0, -padx=>10, -pady=>20);
	$WtSaveBtn = $WtCtlLf->Button(-text => 'Start', -command => sub { WebServiceCtl(); })->pack(-side => 'right',-padx => 5, -pady=>15);	

	$WtLoginLf = $WebTab->Labelframe(-borderwidth=>1, -relief => 'flat', -width => 190 , -height => 15)->pack(-side=>'top', -fill => 'both', -expand => 1, -padx=>10, -pady=>20);
	$WtLoginNameLf = $WtLoginLf->Labelframe(-borderwidth=>1, -relief => 'raised', -text => "Login", -width => 190 , -height => 15)->pack(-side=>'left', -fill => 'x', -expand => 1, -padx=>10, -pady=>20);
	$WtLoginNameEdit = $WtLoginNameLf->Entry(-background => "white", -width=>70)->pack(-side => "left", -fill => 'x', -expand => 1,-padx => 10, -pady=>10);
	$WtLoginPassLf = $WtLoginLf->Labelframe(-borderwidth=>1, -relief => 'raised', -text => "Password", -width => 190 , -height => 15)->pack(-side=>'right', -fill => 'x', -expand => 1, -padx=>10, -pady=>20);
	$WtLoginPassEdit = $WtLoginPassLf->Entry(-background => "white", -width=>70, -show => "*")->pack(-side => "left", -fill => 'x', -expand => 1,-padx => 10, -pady=>10);

	$WtViewLf = $WebTab->Labelframe(-borderwidth=>1, -relief => 'flat', -width => 190 , -height => 15)->pack(-side=>'top', -fill => 'both', -expand => 1, -padx=>10, -pady=>20);
	$WtViewNameLf = $WtViewLf->Labelframe(-borderwidth=>1, -relief => 'raised', -text => "Viewer Login", -width => 190 , -height => 15)->pack(-side=>'left', -fill => 'x', -expand => 1, -padx=>10, -pady=>20);
	$WtViewNameEdit = $WtViewNameLf->Entry(-background => "white", -width=>70)->pack(-side => "left", -fill => 'x', -expand => 1,-padx => 10, -pady=>10);
	$WtViewPassLf = $WtViewLf->Labelframe(-borderwidth=>1, -relief => 'raised', -text => "Viewer Password", -width => 190 , -height => 15)->pack(-side=>'right', -fill => 'x', -expand => 1, -padx=>10, -pady=>20);
	$WtViewPassEdit = $WtViewPassLf->Entry(-background => "white", -width=>70, -show => "*")->pack(-side => "left", -fill => 'x', -expand => 1,-padx => 10, -pady=>10);

	$WtSaveBtn = $WebTab->Button(-text => 'Save', -command => sub { SaveSettings(); })->pack(-side => 'right',-padx => 5, -pady=>15);	

	LoadSettings();

	$ButtonBar = $ReportsTab->Labelframe(-borderwidth=>1, -relief => 'raised', -width => 190 , -height => 15)->pack(-side=>'top', -fill => 'both', -expand => 1, -padx=>10, -pady=>20);	
	$ButtonBar->Button(-text => 'View Daily', -command => sub { $TextBox->insert("end", join("\n", CreateDailyReport)); })->pack(-side => 'left', -padx => 15, -pady=>15);	
	$ButtonBar->Button(-text => 'Send Daily', -command => sub { SendDailyReport; })->pack(-side => 'left', -padx => 15, -pady=>15);	

	$ButtonBar->Button(-text => 'View Weekly', -command => sub { $TextBox->insert("end", join("\n", CreateWeeklyReport)); })->pack(-side => 'left', -padx => 15, -pady=>15);	
	$ButtonBar->Button(-text => 'Send Weekly', -command => sub { SendWeeklyReport; })->pack(-side => 'left', -padx => 15, -pady=>15);	

	$ButtonBar->Button(-text => 'View Monthly', -command => sub { $TextBox->insert("end", join("\n", CreateMonthlyReport)); })->pack(-side => 'left', -padx => 15, -pady=>15);	
	$ButtonBar->Button(-text => 'Send Monthly', -command => sub { SendMonthlyReport; })->pack(-side => 'left', -padx => 15, -pady=>15);	

	$TextBox = $ReportsTab->Text(-height=>30, -width=>60, -background=>'white', -foreground=>'black')->pack(-side=>'bottom', -padx=>5, -pady=>5, -fill => 'both', -expand => 1);

	$ttf = $ToolsTab->Labelframe(-borderwidth=>1, -relief => 'raised', -text => "Use this if you were in a meeting and automatically logged out. Program will exit.", -width => 190 , -height => 15)->pack(-side=>'top', -fill => 'both', -expand => 1, -padx=>10, -pady=>20);
	$ttf->Button(-text => 'Delete last record', -command => sub { SqlExec("DELETE FROM timetrack WHERE id = (SELECT MAX(id) FROM timetrack);"); exit; })->pack(-side => 'left', -padx => 15, -pady=>15);	
}

sub LoadSettings {
	$dreeb->insert('end', $dbh->selectall_arrayref("SELECT value FROM settings WHERE name = 'DailyReportEmailRecipents';")->[0][0]);
	$wreeb->insert('end', $dbh->selectall_arrayref("SELECT value FROM settings WHERE name = 'WeeklyReportEmailRecipents';")->[0][0]);
	$mreeb->insert('end', $dbh->selectall_arrayref("SELECT value FROM settings WHERE name = 'MonthlyReportEmailRecipents';")->[0][0]);

	$mmseb->insert('end', $dbh->selectall_arrayref("SELECT value FROM settings WHERE name = 'MyMailServer';")->[0][0]);
	$meaeb->insert('end', $dbh->selectall_arrayref("SELECT value FROM settings WHERE name = 'MyEmailAddress';")->[0][0]);

	$WtPortEdit->insert('end', $dbh->selectall_arrayref("SELECT value FROM settings WHERE name = 'WebServicePort';")->[0][0]);
	$WtLoginNameEdit->insert('end', $dbh->selectall_arrayref("SELECT value FROM settings WHERE name = 'LoginName';")->[0][0]);
	$WtLoginPassEdit->insert('end', $dbh->selectall_arrayref("SELECT value FROM settings WHERE name = 'LoginPass';")->[0][0]);
	$WtViewNameEdit->insert('end', $dbh->selectall_arrayref("SELECT value FROM settings WHERE name = 'ViewName';")->[0][0]);
	$WtViewPassEdit->insert('end', $dbh->selectall_arrayref("SELECT value FROM settings WHERE name = 'ViewPass';")->[0][0]);
}

sub SaveSettings {	
 	$dbh->prepare("REPLACE INTO settings (name,value) VALUES ('DailyReportEmailRecipents', ?);")->execute($dreeb->get);
 	$dbh->prepare("REPLACE INTO settings (name,value) VALUES ('WeeklyReportEmailRecipents', ?);")->execute($wreeb->get);
 	$dbh->prepare("REPLACE INTO settings (name,value) VALUES ('MonthlyReportEmailRecipents', ?);")->execute($mreeb->get);
 	$dbh->prepare("REPLACE INTO settings (name,value) VALUES ('MyMailServer', ?);")->execute($mmseb->get);
 	$dbh->prepare("REPLACE INTO settings (name,value) VALUES ('MyEmailAddress', ?);")->execute($meaeb->get);
	$dbh->prepare("REPLACE INTO settings (name,value) VALUES ('WebServicePort', ?);")->execute($WtPortEdit->get);

	$dbh->prepare("REPLACE INTO settings (name,value) VALUES ('LoginName', ?);")->execute($WtLoginNameEdit->get);
	$dbh->prepare("REPLACE INTO settings (name,value) VALUES ('LoginPass', ?);")->execute($WtLoginPassEdit->get);
	$dbh->prepare("REPLACE INTO settings (name,value) VALUES ('ViewName', ?);")->execute($WtViewNameEdit->get);
	$dbh->prepare("REPLACE INTO settings (name,value) VALUES ('ViewPass', ?);")->execute($WtViewPassEdit->get);
}

sub WebServiceCtl {
	printf "WebServiceCtl\n";
	chdir($AppPath);
	system("./TimeTrackerWeb.pl &");
}

sub CheckPunchStatus {
	print "CheckPunchStatus\n";
	my $LastActivity = InOrOut();
	if ( $LastActivity eq "Out" ) {
		if ($AutoCron == 1) { Destructor(); }
		$ioHelp->configure(-text => "You are out, please punch in.");
		$be->configure(-state=> 'disabled');
		$ob->configure(-state=> 'disabled');
		$eb->configure(-state=> 'disabled');
		$pb->configure(-state=> 'disabled');
		$rlb->configure(-state=> 'disabled');	
	} else {
		$ioHelp->configure(-text => "You are in.");
		$ib->configure(-state=> 'disabled');
	}
}

sub Destructor {
	print "Destructor\n";
	Calculations();
	if(defined($dbh)) {
		$dbh->disconnect();
	} 
	exit;
}

sub SaveActivity {
	print "SaveActivity\n";
	my $Project = $be->get('active');
	$Project =~ s/^\s+//;
	$Project =~ s/\s+$//;
	if($Project eq "Other...") {
		# $id->LabEntry
		if ( $id->Show() eq "Ok" ) {
			my $NewName = $idEntry->get();
			print "NewName = '$NewName'\n";
			$NewName =~ s/^\s+//;
			$NewName =~ s/\s+$//;
			$Project = $NewName;
		}
	}
	#print "Could be = '".$be->get()."'\n";
	print "Project='$Project'\n";
	my $Activity = $eb->get();
	$Activity =~ s/^\s+//;
	print "Activity before='$Activity'\n";
	$Activity =~ s/\s+$//;
	$Activity =~ s/[^[:ascii:]]//g;
	$Activity =~ s/''/'/g;
	print "Activity afer='$Activity'\n";
	
	my $dt = DateTime->now();
	my $Now = $dt->epoch();
	printf "Now = '$Now'\n";
	if(($Project ne "") && ($Activity ne "")) {
		#my $dbh = DBI->connect("dbi:SQLite:dbname=$DbFile", "", "", {RaiseError =>1}) or die $DBI::errstr;
		if($Debug == 1) { 
			printf "INSERT INTO timetrack (id, project, activity, time) VALUES ((SELECT MAX(id)+1 FROM timetrack), '%s', '%s', '%s');\n", $Project, $Activity, $Now;
		}
		$dbh->prepare("INSERT INTO timetrack (id, project, activity, time) VALUES ((SELECT MAX(id)+1 FROM timetrack), ?, ?, ?);")->execute($Project, $Activity, $Now);
		ResetFailedToEnterCount();
		#$dbh->disconnect();
	}
}


sub DisplayHoursWorked {
	my $StartDate = DateTime->now->set_time_zone($MyTimeZone);
	$StartDate->truncate( to => 'week');
	my $EndDate = DateTime->from_object(object => $StartDate);
	$EndDate->add(days => 1);
	$EndDate->subtract(seconds => 1);
	my $set = $StartDate->epoch();
	my $eet = $EndDate->epoch();

	my $DayTotal = 0;
	my $WeekTotal = 0;
	while ($StartDate < DateTime->now->set_time_zone($MyTimeZone)) {
		# print "SELECT MAX(dst) FROM timetrack WHERE project != 'PUNCH' AND time > $set AND time < $eet;\n";
		$DayTotal = $dbh->selectall_arrayref("SELECT MAX(dst) FROM timetrack WHERE project != 'PUNCH' AND time > $set AND time < $eet;")->[0][0];

		if ( defined($DayTotal)) {
			$WeekTotal += $DayTotal;
			# print "WeekTotal = $WeekTotal\t DayTotal = $DayTotal\n";
		}
		$StartDate->add(days => 1);
		$EndDate->add(days => 1);
		$set = $StartDate->epoch();
		$eet = $EndDate->epoch();
	}
	my $Hours = 0; 
	my $Minutes = 0;
	my $Seconds = 0;
	if (defined($DayTotal)) {
		$Hours = int ($DayTotal / 60 / 60);
		$Minutes = int (($DayTotal - ($Hours * 60 * 60)) / 60);
		$Seconds = ($DayTotal - ($Hours * 60 * 60) - ($Minutes * 60));
		# printf "today %01d:%02d:%02d\n", $Hours, $Minutes, $Seconds;
	}

	$TodayTime->configure(-text => "Today: ".sprintf "%01d:%02d:%02d", $Hours, $Minutes, $Seconds);
	if ($WeekTotal > 0) {
		$Hours = int ($WeekTotal / 60 / 60);
		$Minutes = int (($WeekTotal - ($Hours * 60 * 60)) / 60);
		$Seconds = ($WeekTotal - ($Hours * 60 * 60) - ($Minutes * 60));
	}
	$WeekTime->configure(-text => "This week: ".sprintf "%01d:%02d:%02d", $Hours, $Minutes, $Seconds);
	# printf "Week %01d:%02d:%02d\n", $Hours, $Minutes, $Seconds;
}


sub CalculateLastActivityTime {
	#my $dbh = shift;
	my $LastTime = $dbh->selectall_arrayref("SELECT time FROM timetrack WHERE id = (SELECT MAX(id) FROM timetrack) AND activity != 'In' AND activity != 'Out';")->[0][0];
	if (defined($LastTime)) {
		my $NowTime = DateTime->now->epoch();
		my $NextTime = $LastTime + (60 * 15);
		if ( $NextTime > $NowTime ) {
			#print "Not yet\n";
			Destructor();
		}
	}
}

sub OnlyOne {
	if (defined $ENV{'LOCKFILE'}) {
		printf "Using flock on $ENV{'LOCKFILE'}\n";
		open (FH, "<", $ENV{'LOCKFILE'});
		unless (flock(FH, LOCK_EX|LOCK_NB)) {
			print "$0 is already running. Exiting on line ".__LINE__.".\n";
			#if($Debug == 0) {
			#	exit(1);
			#}
		}
	} else {
		unless (flock(DATA, LOCK_EX|LOCK_NB)) {
			print "$0 is already running. Exiting on line ".__LINE__.".\n";
			if($Debug == 0) {
				exit(1);
			}
		}
	}
}


__DATA__
This exists so flock() code above works.
DO NOT REMOVE THIS DATA SECTION.

