#!/bin/bash

PATH=/bin/
APP_PATH=$(dirname $0)
cd $APP_PATH
./LookWhatIDidAtWorkToday.pl $@

# Put lwp.bat in the Windows path to quickly run from the start menu.
# If you are the only user of the Windows system, add lwp.bat to C:\Windows\ 

# Create a Task Scheduler event to run every minute
# Set, "Run whether user is logged on or not"
# Check Hidden
# Action: Start a program
# Program: C:\cygwin64\bin\run 
# Add arguments: -p /bin bash "/cygdrive/c/Users/Royce/Devel/Subversion/TimeTracker/lwp.sh" -a

# The user account needs to have "log on as a batch job" permission
# https://docs.microsoft.com/en-us/windows/security/threat-protection/security-policy-settings/log-on-as-a-batch-job