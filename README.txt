This program provides a very easy way to track what projects you work on
during the day. It will produce daily activity reports with detailed time
stamps, weekly project summary reports and monthly project summary reports.

Sqlite Database Browser http://sqlitebrowser.sourceforge.net/

Linux install
-------------------------------------------------------
mkdir ~/tmp/
mkdir ~/bin/
cd ~/bin/
# Copy lwx and LookWhatIDidAtWorkToday.pl in to ~/bin
ln -s LookWhatIDidAtWorkToday.pl lw
crontab -e
# Add a line like
# * * * * * bin/lwx -a >tmp/lw.log 2>&1

# Install packages for Red Hat
yum install perl-DateTime-Format-Strptime perl-File-HomeDir perl-DBD-SQLite perl-Mail-Box perl-User-Identity

# Install packages for Debian
apt-get install libdatetime-format-strptime-perl libdbi-perl libfile-homedir-perl libdbd-sqlite3-perl






Windows install with ActiveState
-------------------------------------------------------
# Install ActiveState Perl 5.16
# Install these packages Tk DateTime DateTime-Format-Strptime
# Copy lwx and LookWhatIDidAtWorkToday.pl in to some place in your home directroy.
# Open "Control Pannel"
# Search for "Schedule Tasks"
# Create a new task, set it to run once every minute this command
# wperl LookWhatIDidAtWorkToday.pl -a 


Windows install with Cygwin
-------------------------------------------------------
This works well but I have not documented the steps yet.

