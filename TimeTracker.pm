package TimeTracker;


# =============================================================================
#
#    Copyright (C) 2019 Silicon Tao Technology Systems Inc.
#    E-mail:  support@SiliconTao.com
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#=============================================================================

use parent 'Exporter';
use DateTime;
use DateTime::Format::Strptime qw( );
use DBI;
use Net::SMTP;
use Sys::Hostname;
use POSIX;
use File::HomeDir;

our $Debug = 0; # Create the file ~/Documents/LWIDAWT.debug to enable
our $DbFile;
our $dbh;
our $sth;
our $UserName;
our $OurDomain = "ourdomain.fake";
our $EmailSuccess = 0;
our $MyTimeZone = "America/Regina";
use Config;
#print "$Config{osname}\n";
#print "$Config{archname}\n";
#exit; 
our $BlankChar = 9;
if( ($Config{osname} eq "linux") or ($Config{osname} eq "darwin") ) {
 $UserName = $ENV{'USER'};
} else
{
	require Win32;
	# require DateTime::TimeZone::Local::Win32;
	$UserName = Win32::LoginName;
	$BlankChar = 1;
}
our @EXPORT = qw($Debug $DbFile $dbh $sth $UserName $OurDomain $EmailSuccess $MyTimeZone $BlankChar 
	SetLastHostIn EmailReport CreateReports SendDailyReport SendWeeklyReport SendMonthlyReport 
	CreateDailyReport CreateWeeklyReport CreateMonthlyReport SqlExec InOrOut Calculations 
	ContineWork SaveInOut CalculateEt CalculatePttm CalculatePttw CalculatePttd CalculateDst CreateDb 
	CreateSettingsTable RecordEvent CreateEventLogTable CreateReportLogTable CreateTimeTrackTable 
	ResetFailedToEnterCount GetTimeToday GetTimeThisWeek);


sub SetLastHostIn {
	# hostname
	my $LastHostIn = $dbh->selectall_arrayref("SELECT value FROM settings WHERE name = 'LastHostIn';")->[0][0];
	if(! defined($LastHostIn)) {
		$dbh->prepare("INSERT INTO settings (name, value) VALUES ('LastHostIn', 'first');")->execute;
	}
	$dbh->prepare("UPDATE settings SET value='".hostname()."' WHERE name = 'LastHostIn';")->execute;	
}

sub EmailReport {
	# Disable reports for now. They run constantly. Should only run once a day.
	return;

	my ($Subject, $Report, $SendTo) = @_;
	# my $MyAddress = $UserName.'@'.$OurDomain;
	my $MyAddress = $dbh->selectall_arrayref("SELECT value FROM settings WHERE name = 'MyEmailAddress';")->[0][0];
	#	my $PlainText = 1;
	#	my $HtmlText = 0;
	#	my $MessageBody = "";
	#	my $MessageContents = "";
	#	my $AttachFiles = "";
	
	# Only works from Royce's home ISP 
	#my $SMTPServer = "mail.shaw.ca";

	# Trying this again after access reported a lot of email erros.
	my $SMTPServer = $dbh->selectall_arrayref("SELECT value FROM settings WHERE name = 'MyMailServer';")->[0][0];
	# my $SMTPServer = "mail.$OurDomain";
	print "SMTPServer = $SMTPServer\n";

	# Tried this from when I am at the hotel in Regina. Better to send from my office desktop.
	#my $SMTPServer = "smtp.sasktel.com";

	# Access is reporting email is bouncing with rejection messages when sending to people on Exchange 365, but the people still get the emails.
	#my $SMTPServer = "smtp.accesscomm.ca";
	
	#if($MessageBodyFile gt "") {
	#	if(-f $MessageBodyFile)  {
	#		$MessageBody = `cat $MessageBodyFile`;
	#	} else {
	#		die("Cannot find that file $MessageBodyFile");
	#	}
	#}
	
	#if($HtmlText) {
	#	print "Sending as HTML message\n";
	#	$MessageType = "text/html";	
	#	$MessageContents = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 TRANSITIONAL//EN\">\n";
	#	$MessageContents .= "<HTML>\n<HEAD>\n<META HTTP-EQUIV=\"Content-Type\" CONTENT=\"text/html; CHARSET=UTF-8\">\n";
	#	$MessageContents .= "<META NAME=\"GENERATOR\" CONTENT=\"GtkHTML/3.0.7\">\n";
	#	$MessageContents .= "</HEAD>\n<BODY>\n";
	#	$MessageContents .= $MessageBody;
	#	$MessageContents .= "</BODY>\n</HTML>\n\n";
	#} else {
	#	$MessageType = "text/plain";	
	#	$MessageContents = $Report."\n\n From host=". hostname();
	#}
	
	#if($AttachFiles gt "") {
	#	@BodyParts = ();
	#	for $EachAttachment (@Attachments) {
	#		$Fbn = basename($EachAttachment);
	#		$BodyPart = Mail::Message::Body->new(file => $EachAttachment);
	#		$encodedBody = $BodyPart->encode(transfer_encoding => 'base64');
	#		$encodedBody->disposition("attachment; filename='".$Fbn."';");
	#		push(@BodyParts, $encodedBody);
	#	}
	#	$DataBody = Mail::Message::Body->new(mime_type => $MessageType, data => $MessageContents);
	#	$MessageBody = Mail::Message::Body::Multipart->new(bountry => time.'--it-s-mime', parts => [$DataBody, @BodyParts]);
	#} else
	#{
	#	$MessageBody = Mail::Message::Body->new(mime_type => $MessageType, data => $MessageContents);		
	#}
	#	$NewMessage = Mail::Message->buildFromBody($MessageBody, From => $MyAddress, To => $SendTo, Subject => $Subject);
	#	$Connection = Mail::Transport::SMTP->new(hostname => $SMTPServer, timeout => 30);
	#	my $obj = Mail::Reporter->new($Connection);
	#	if (my $Result = $Connection->send($NewMessage)) {
	#		print "Message sent.\n";
	#	} else {
	#		print "Error sending message!\n";
	#		print "Erros are ".$obj->errors()."\n----\n";
	#	}
	# my $NewSubject = hostname()." Beta $Subject";
	my $SendFailed = 0;
	my $smtp = Net::SMTP->new($SMTPServer, Timeout =>15, Debug => 1) or die "Cannot connect to SMTP = \"$SMTPServer\"";
	print "SMTP domain = ".$smtp->domain()."\n";
	#$smtp->mail($MyAddress);
	$smtp->mail($MyAddress);
	$smtp->to($SendTo) or $SendFailed = 1;
	$smtp->data();
	$smtp->datasend("To: $SendTo\n");
	$smtp->datasend("From: $MyAddress\n");
	$smtp->datasend("Subject: $Subject\n");
	$smtp->datasend("\n");
	$smtp->datasend($Report."\n\n From host=". hostname());
	$smtp->dataend();
	$smtp->quit;
	
	print "SendFailed = $SendFailed\n";
	if($SendFailed == 0) {
		$EmailSuccess = 1;
		print "Send worked\n";
		RecordEvent("Email sent, subject=$Subject");
	} else {
		print "Send did not work\n";
	}
}

sub CreateReports {
	print "CreateReports\n";
	my $CurrentTimeObject = DateTime->now->set_time_zone($MyTimeZone);
	my $CurrentTimeInt = $CurrentTimeObject->epoch();
	my $CutOffTime = $CurrentTimeObject->clone->truncate(to => 'day');
	my $LastReport = $dbh->selectall_arrayref("SELECT lasttime FROM reportlog WHERE report = 'daily';")->[0][0];
	print "LastReport=$LastReport, CutOffTime=".$CutOffTime->epoch()."\n";
	if ($CutOffTime->epoch() > $LastReport) {
		SendDailyReport();
		if($EmailSuccess == 1) {
			$dbh->do("UPDATE reportlog SET lasttime=$CurrentTimeInt WHERE report = 'daily';");
		}
	}

	$CutOffTime = $CurrentTimeObject->clone->truncate(to => 'week');
	$LastReport = $dbh->selectall_arrayref("SELECT lasttime FROM reportlog WHERE report = 'weekly';")->[0][0];
	if ($CutOffTime->epoch() > $LastReport) {
		SendWeeklyReport();
		if($EmailSuccess == 1) {
			$dbh->do("UPDATE reportlog SET lasttime=$CurrentTimeInt WHERE report = 'weekly';");
		}
	}

	$CutOffTime = $CurrentTimeObject->clone->truncate(to => 'month');
	$LastReport = $dbh->selectall_arrayref("SELECT lasttime FROM reportlog WHERE report = 'monthly';")->[0][0];
	if ($CutOffTime->epoch() > $LastReport) {
		SendMonthlyReport();
		if($EmailSuccess == 1) {
			$dbh->do("UPDATE reportlog SET lasttime=$CurrentTimeInt WHERE report = 'monthly';");
		}
	}
}

sub SendDailyReport {
	print "SendDailyReport\n";
	my $Recipents;
	$EmailSuccess = 0;
	$Recipents = $dbh->selectall_arrayref("SELECT value FROM settings WHERE name = 'DailyReportEmailRecipents';")->[0][0];
	if($Recipents ne "") {
		print "do something to email the report now.\n";
		EmailReport(CreateDailyReport(), $Recipents);
	}
}

sub SendWeeklyReport {
	print "SendWeeklyReport\n";
	my $Recipents;
	$EmailSuccess = 0;
	$Recipents = $dbh->selectall_arrayref("SELECT value FROM settings WHERE name = 'WeeklyReportEmailRecipents';")->[0][0];
	if($Recipents ne "") {
		EmailReport(CreateWeeklyReport(), $Recipents);
	}
}

sub SendMonthlyReport {
	print "SendMonthlyReport\n";
	$EmailSuccess = 0;
	$Recipents = $dbh->selectall_arrayref("SELECT value FROM settings WHERE name = 'MonthlyReportEmailRecipents';")->[0][0];
	if($Recipents ne "") {	
		EmailReport(CreateMonthlyReport(), $Recipents);
	}
}

sub CreateDailyReport {	
	print "CreateDailyReport\n";
	my $StartDate = DateTime->now->set_time_zone($MyTimeZone)->truncate(to => 'day');
	$StartDate->subtract(days => 1);
	my $EndDate = DateTime->from_object(object => $StartDate);
	$EndDate->add(days => 1);
	$EndDate->subtract(seconds => 1);
	my $set = $StartDate->epoch();
	my $eet = $EndDate->epoch();
	my $Report = "";
	my $Subject;
	$Subject = "Daily Activity Report for ".$StartDate->day_name()." ".$StartDate->month_name()." ".$StartDate->day().", ".$StartDate->year();
	my $SqlCmd = "SELECT MAX(dst) FROM timetrack WHERE time > $set AND time < $eet;";
	print "SqlCmd='$SqlCmd'\n";
	my $Seconds = $dbh->selectall_arrayref($SqlCmd)->[0][0];
	if(defined($Seconds)) {
		my $ConvertTime = DateTime::Format::Strptime->new(pattern => '%s', on_error => 'croak')->parse_datetime($Seconds);		
		
		$Report .= sprintf "Total Daily Service Time = %d:%02d\n", $ConvertTime->hour(), $ConvertTime->minute();
		$Report .= "-------------------------------------------------------\n";

		my $sth = $dbh->prepare("SELECT id, project, activity, time FROM timetrack WHERE time > $set AND time < $eet ORDER BY id DESC;");
		$sth->execute();
		$Report .= sprintf "%-20s | %-30s | %-100s\n", 'Time stamp', 'Project', 'Activity';

		while (my @data = $sth->fetchrow_array()) {
			my $ConvertDate = DateTime::Format::Strptime->new(time_zone => $MyTimeZone, pattern => '%s', on_error => 'croak')->parse_datetime($data[3]);
			$Report .= sprintf "%-20s | %-30s | %-100s\n", $ConvertDate->date()." ".$ConvertDate->time(), $data[1], $data[2];
		}	

		print "report = '$Report'\n";
		#if($Debug == 1) {
		#	EmailReport($Report, $Subject, 'osgnuru@gmail.com');
		#} else {
		#	EmailReport($Report, $Subject, $Recipents);
		#}
		return($Subject, $Report);
	}
}

sub CreateWeeklyReport {
	print "CreateWeeklyReport\n";
	my $StartDate = DateTime->now->set_time_zone($MyTimeZone)->truncate(to => 'week')->subtract( days => 1)->truncate(to => 'week');
	$StartDate->truncate( to => 'week');
	my $EndDate = DateTime->from_object(object => $StartDate);
	$EndDate->add(days => 7);
	$EndDate->subtract(seconds => 1);
	my $set = $StartDate->epoch();
	my $eet = $EndDate->epoch();
	my $Report = "";
	my $Subject = "Weekly Activity Report from ".$StartDate->month_name()." ".$StartDate->day().", ".$StartDate->year()." to ".$EndDate->month_name()." ".$EndDate->day().", ".$EndDate->year()."\n";
	my $TotalSeconds = 0;
	my $SqlCmd = "SELECT DISTINCT(project),MAX(pttw) FROM timetrack WHERE project != 'PUNCH' AND time > $set AND time < $eet GROUP BY project ORDER BY pttw DESC;";
	if ( $Debug == 1 ) { 
		print $SqlCmd."\n";
	}
	my $sth = $dbh->prepare($SqlCmd);
	$sth->execute();
	$Report .= sprintf "%-50s | %-20s\n", 'Project', 'Hours:Minutes:Seconds';

	while (my @data = $sth->fetchrow_array()) {
		$TotalSeconds += $data[1];
		my $Hours = int ($data[1] / 60 / 60);
		my $Minutes = int (($data[1] - ($Hours * 60 * 60)) / 60);
		my $Seconds = ($data[1] - ($Hours * 60 * 60) - ($Minutes * 60));
		# printf "D Seconds %d = (data %d - (Hours %d * 60 * 60) - (Minutes %d * 60))\n", $Seconds, $data[1], $Hours, $Minutes;
		$Report .= sprintf "%-50s | %01d:%02d:%02d\n", $data[0], $Hours, $Minutes, $Seconds;
	}

	my $Hours = int ($TotalSeconds / 60 / 60);
	my $Minutes = int (($TotalSeconds - ($Hours * 60 * 60)) / 60);
	my $Seconds = ($TotalSeconds - ($Hours * 60 * 60) - ($Minutes * 60));
	$Report .= sprintf "\n\nTotal hours:minutes this week = %01d:%02d:%02d\n", $Hours, $Minutes, $Seconds;

	if( $Hours + $Minutes + $Seconds > 0) {
		#if($Debug == 1) {
		#	EmailReport($Report, $Subject, 'osgnuru@gmail.com');
		#} else {
		#	EmailReport($Report, $Subject, $Recipents);
		#}
		print "report = '$Report'\n";
		return($Subject, $Report);
	}	
}

sub CreateMonthlyReport {
	print "CreateMonthlyReport now\n";
	my $StartDate = DateTime->now->set_time_zone($MyTimeZone)->truncate(to => 'month')->subtract( days => 1)->truncate(to => 'month');
	$StartDate->truncate( to => 'month');
	my $EndDate = DateTime->from_object(object => $StartDate);
	$EndDate->add(months => 1);
	$EndDate->subtract(seconds => 1);
	my $set = $StartDate->epoch();
	my $eet = $EndDate->epoch();
	my $Report = "";
	my $Subject = "Monthly Activity Report from ".$StartDate->month_name()." ".$StartDate->day()." to ".$EndDate->month_name()." ".$EndDate->day().", ".$EndDate->year()."\n";
	my $TotalSeconds = 0;
	my $SqlCmd = "SELECT DISTINCT(project),MAX(pttm) FROM timetrack WHERE project != 'PUNCH' AND time > $set AND time < $eet GROUP BY project ORDER BY pttm DESC;";
	print "SqlCmd = $SqlCmd\n";

	my $sth = $dbh->prepare($SqlCmd);
	$sth->execute();
	$Report .= sprintf "%-50s | %-20s\n", 'Project', 'Hours:Minutes';

	while (my @data = $sth->fetchrow_array()) {
		$TotalSeconds += $data[1];
		my $Hours = int ($data[1] / 60 / 60);
		my $Minutes = int (($data[1] - ($Hours * 60 * 60)) / 60);
		my $Seconds = ($data[1] - ($Hours * 60 * 60) - ($Minutes * 60));
		$Report .= sprintf "%-50s | %01d:%02d:%02d\n", $data[0], $Hours, $Minutes, $Seconds;
	}

	my $Hours = int ($TotalSeconds / 60 / 60);
	my $Minutes = int (($TotalSeconds - ($Hours * 60 * 60)) / 60);
	my $Seconds = ($TotalSeconds - ($Hours * 60 * 60) - ($Minutes * 60));
	$Report .= sprintf "\n\nTotal hours:minutes this month = %01d:%02d:%02d\n", $Hours, $Minutes, $Seconds;
	
	if( $Hours + $Minutes + $Seconds > 0) {
		#if($Debug == 1) {
		#	EmailReport($Report, $Subject, 'osgnuru@gmail.com');
		#} else {
		#	EmailReport($Report, $Subject, $Recipents);	
		#}
		print "report = '$Report'\n";		
		return($Subject, $Report);
	}
}

sub SqlExec {
	my $SqlCmd = shift;
	$sth = $dbh->prepare($SqlCmd);
	$sth->execute();	
}

sub InOrOut {
	my $TempReturn = $dbh->selectall_arrayref("SELECT activity FROM timetrack WHERE id = (SELECT MAX(id) FROM timetrack);")->[0][0];
	if($TempReturn ne "Out") {
		$TempReturn = "In";
	}
	return($TempReturn);
}

sub Calculations {
	CalculateEt();
	CalculateDst();
	CalculatePttd();
	CalculatePttw();
	CalculatePttm();
	print "Done wtih calculations\n";
}

sub ContineWork {
	my ($DontCare, $Activity) = split(chr($BlankChar), shift);
	$Activity =~ s/^\s+//;
	$Activity =~ s/\s+$//;
	my $dt = DateTime->now();
	my $Now = $dt->epoch();
	my $CleanActivity = $Activity;
	print "Activity = '$Activity'\n";
	$CleanActivity =~ s/[^[:ascii:]]//g;
	printf "CleanActivity = '$CleanActivity'\n";
	while ( $CleanActivity =~ s/''/'/g ) {
		#printf "CleanActivity = '$CleanActivity'\n";
		sleep 1
	}
	printf "CleanActivity = '$CleanActivity'\n";
	#printf "LINE = %d\n", __LINE__;


	#my @row = $dbh->selectrow_array("SELECT MAX(time),project FROM timetrack WHERE activity = '$CleanActivity';");
	my $sth = $dbh->prepare("SELECT MAX(time),project FROM timetrack WHERE activity = ?;");
	$sth->execute($CleanActivity);
	my @row = $sth->fetchrow_array;
	#printf "LINE = %d\n", __LINE__;
	#print "time = '".$row[0]."'\n";
	if(defined($row[1])) {
		#print "project = '".$row[1]."'\n";
		my $Project = $row[1];
		#printf "LINE = %d\n", __LINE__;
		$dbh->prepare("INSERT INTO timetrack (id, project, activity, time) VALUES ((SELECT MAX(id)+1 FROM timetrack), ?, ?, ?);")->execute($Project, $CleanActivity, $Now);
		#printf "LINE = %d\n", __LINE__;
	} else {
		#printf "LINE = %d\n", __LINE__;
		$dbh->prepare("INSERT INTO timetrack (id, activity, time) VALUES ((SELECT MAX(id)+1 FROM timetrack), ?, ?);")->execute($CleanActivity, $Now);
		#printf "LINE = %d\n", __LINE__;
	}
}

sub SaveInOut {
	print "SaveInOut\n";
	my $Activity = shift;
	my $CleanActivity = $Activity;
	$CleanActivity =~ s/[^[:ascii:]]//g;
	$CleanActivity =~ s/'/''/g;
	my $dt = DateTime->now();
	my $Now = $dt->epoch();
	#my $dbh = DBI->connect("dbi:SQLite:dbname=$DbFile", "", "", {RaiseError =>1}) or die $DBI::errstr;
	if($Activity eq "Out") {
		#  "INSERT INTO timetrack (id, project, activity, time, et) SELECT $NextId2Fix+1, project, activity, time+1, 1 FROM timetrack WHERE id=$NextId2Fix;";		
		my $SqlCmd = "INSERT INTO timetrack (id, project, activity, time) SELECT id+1, project, activity, $Now-1 FROM timetrack ORDER BY id DESC LIMIT 1;";
		print "$SqlCmd\n";
		$dbh->prepare($SqlCmd)->execute();
	} else {
		SetLastHostIn();
	} 
	$dbh->prepare("INSERT INTO timetrack (id, project, activity, time) VALUES ((SELECT MAX(id)+1 FROM timetrack), 'PUNCH', ?, ?);")->execute($CleanActivity, $Now);
	ResetFailedToEnterCount();
	#$dbh->disconnect();
}

sub CalculateEt {
	#my $dbh = shift;
	print "CalculateEt\n";
	my $LastTime = 0;
	my $NextTime = 0;
	my $StartId = 0;
	my $DeltaTime = 0;
	my $StartDate = DateTime->now();
	$StartDate->truncate( to => 'day');
	# This is to fix yesterdays time if something went wrong
	#$StartDate->subtract( days => 1);
	my $StartEpoch = $StartDate->epoch();
	my @HashRef = $dbh->selectrow_array("SELECT MIN(id) FROM timetrack WHERE time > $StartEpoch AND et IS NULL;");
	if(defined($HashRef[0])) {
		$StartId = $HashRef[0];
		if($HashRef[0] > 1)
		{
			@HashRef = $dbh->selectrow_array("SELECT time FROM timetrack WHERE id=$StartId - 1;");
			$LastTime = $HashRef[0];
		}
		@HashRef = $dbh->selectrow_array("SELECT time FROM timetrack WHERE id=$StartId;");
		$NextTime = $HashRef[0];
		while($NextTime ne "") {
			if($LastTime > 0) {
				#print "LastTime = '$LastTime'\n";
				#print "NextTime = '$NextTime'\n";
				$DeltaTime = $NextTime - $LastTime;
				if ($DeltaTime > 7200) {
					#print "Missed puch out, adjust to 2 hours. ID=$StartId\n";
					$DeltaTime = 7200;
				}
				#print "UPDATE timetrack SET et=$DeltaTime WHERE id=$StartId;\n";
				$dbh->prepare("UPDATE timetrack SET et=$DeltaTime WHERE id=$StartId;")->execute();
			}
			$StartId++;
			$LastTime = $NextTime;
			$NextTime = "";
			@HashRef = $dbh->selectrow_array("SELECT time FROM timetrack WHERE id=$StartId;");
			if(defined($HashRef[0])) {
				$NextTime = $HashRef[0];
			}
		}
	}
	print "\tDone CalculateEt\n";
}

sub CalculatePttm {
	print "CalculatePttm\n";
	my $StartMonth = DateTime->now->set_time_zone($MyTimeZone);
	$StartMonth->truncate( to => 'month');
	my $som = $StartMonth->epoch();
	#print "SELECT MIN(time) FROM timetrack WHERE pttm IS NULL AND project != 'PUNCH' AND time > $som;\n";
	my $StartTimeInt = $dbh->selectall_arrayref("SELECT MIN(time) FROM timetrack WHERE pttm IS NULL AND project != 'PUNCH' AND time > $som;")->[0][0];
	if(defined($StartTimeInt)) {
		my $StartDate = DateTime::Format::Strptime->new(pattern => '%s', on_error => 'croak')->parse_datetime($StartTimeInt);
		my $EndDate = DateTime->from_object(object => $StartMonth);
		$EndDate->add(months => 1);
		$EndDate->subtract(seconds => 1);
		my $Done = 10;
		while ($Done > 0) {
			my $set = $StartDate->epoch();
			my $eet = $EndDate->epoch();
			#print "Month of ".$StartDate->date()." to ".$EndDate->date()."\n";
			my %Totals = ();
			my $SqlCmd = "SELECT id, project, et, activity, pttm FROM timetrack WHERE et NOT NULL AND time >= $set AND time < $eet ORDER BY id;";
			#print "$SqlCmd\n";
			$sth = $dbh->prepare($SqlCmd);
			$sth->execute();
			my @previousData;
			$Done--;
			while (my @data = $sth->fetchrow_array()) {
				$Done = 2;
 				if(!defined($Totals{$data[1]})) {
					$Totals{$data[1]} = $dbh->selectall_arrayref("SELECT MAX(pttm) FROM timetrack WHERE project='$data[1]' AND time >= $som;")->[0][0] or $Totals{$data[1]} = 0;
					#print "New project this sample $data[1] set to $Totals{$data[1]}\n";					
				}
				if(($data[3] eq "Out") and (defined($previousData[0]))) {
					$Totals{$previousData[1]} += $data[2];
					#print " + UPDATE timetrack SET pttm=$Totals{$previousData[1]} WHERE id=$previousData[0] AND project='$previousData[1]'\tdata=$data[2]\n";
					my $up = $dbh->prepare("UPDATE timetrack SET pttm=? WHERE id=?;");
					my $result = $up->execute($Totals{$previousData[1]}, $previousData[0]);
				} else {
					$Totals{$data[1]} += $data[2];
					if(! defined($data[4])) {
						#print " - UPDATE timetrack SET pttm=$Totals{$data[1]} WHERE id=$data[0] AND project='$data[1]'\tdata=$data[2]\n";
						my $up = $dbh->prepare("UPDATE timetrack SET pttm=? WHERE id=?;");
						my $result = $up->execute($Totals{$data[1]}, $data[0]);
					}
				}
				@previousData = @data;
			}
			$StartDate->add(months => 1);
			$EndDate->add(months => 1);
		}
	}
	print "\tDone CalculatePttm\n";
}

sub CalculatePttw {
	print "CalculatePttw\n";
	my $StartDate = DateTime->today()->truncate( to => 'week' );
	$StartDate->truncate( to => 'week');	
	#$StartDate->subtract(weeks => 1);
	my $sow = $StartDate->epoch();
	my $SqlCmd = "SELECT MIN(time) FROM timetrack WHERE pttw IS NULL AND project != 'PUNCH' AND time > $sow;";
	#print "$SqlCmd\n";
	my $StartTimeInt = $dbh->selectall_arrayref($SqlCmd)->[0][0];
	if(defined($StartTimeInt)) {
		#print "StartTimeInt=$StartTimeInt\n";
		# We need to start at a pttw that needs to be updated, that is what StartTimeInt is for. Otherwise we end up recalculating all pttw's from Oct 28, 2013
		$StartDate = DateTime::Format::Strptime->new(pattern => '%s', on_error => 'croak')->parse_datetime($StartTimeInt);
		# We need to get the EndDate truncated to the next week.
		#$StartDate->truncate( to => 'week');	
		my $EndDate = DateTime->from_object(object => $StartDate);
		$EndDate->truncate( to => 'week');
		$EndDate->add(weeks => 1);
		$EndDate->subtract(seconds => 1);
		my $Done = 10;
		while ($Done > 0) {
			#print "Project Time This Week report from ".$StartDate->day_name()." ".$StartDate->month_name()." ".$StartDate->day().", ".$StartDate->year()." to ".$EndDate->day_name()." ".$EndDate->month_name()." ".$EndDate->day().", ".$EndDate->year()."\n";
			my $set = $StartDate->epoch();
			my $StartOfWeek = $StartDate;
			$StartOfWeek->truncate( to => 'week');
			$sow = $StartOfWeek->epoch();
			my $eet = $EndDate->epoch();
			print "Week of ".$StartDate->date()." to ".$EndDate->date()."\n";
			my %Totals = ();
			$SqlCmd = "SELECT id, project, et, activity, pttw FROM timetrack WHERE project != 'PUNCH' AND et NOT NULL AND time >= $set AND time < $eet ORDER BY time;";
			print "$SqlCmd\n";			
			$sth = $dbh->prepare($SqlCmd);
			$sth->execute();
			my @previousData;
			$Done--;
			while (my @data = $sth->fetchrow_array()) {
				$Done = 2;
				print "Working on id=$data[0], project='$data[1]', et=$data[2]\n";
 				if(!defined($Totals{$data[1]})) {
					$SqlCmd = "SELECT MAX(pttw) FROM timetrack WHERE project='$data[1]' AND time > $sow AND time < $eet;";
					print "$SqlCmd\n";					
					$Totals{$data[1]} = $dbh->selectall_arrayref($SqlCmd)->[0][0] or $Totals{$data[1]} = 0;
					print "New project this sample $data[1] set to $Totals{$data[1]}\n";				
				}
				if(($data[3] eq "Out") and (defined($previousData[0]))) {
					print "+ Totals{$previousData[1]} += $data[2];\n";
					$Totals{$previousData[1]} += $data[2];
					print "+ Totals{$previousData[1]} = $Totals{$previousData[1]};\n";
					#my $DbValue = $dbh->selectall_arrayref("SELECT pttw FROM timetrack WHERE id=".$previousData[0].";")->[0][0];
					#print "Totals=".$Totals{$previousData[1]}.",\tDbValue=".$DbValue."\n";
					#if($Totals{$previousData[1]} ne $DbValue) {
						print " + UPDATE timetrack SET pttw=$Totals{$previousData[1]} WHERE id=$previousData[0]; ( project='$previousData[1]' data=$data[2] )\n";
						my $up = $dbh->prepare("UPDATE timetrack SET pttw=? WHERE id=?;");
						my $result = $up->execute($Totals{$previousData[1]}, $previousData[0]);
					#}
				} else {
					print "- Totals{$data[1]} += $data[2];\n";			
					$Totals{$data[1]} += $data[2];
					print "+ Totals{$data[1]} = $Totals{$data[1]};\n";
					#my $DbValue = $dbh->selectall_arrayref("SELECT pttw FROM timetrack WHERE id=".$data[0].";")->[0][0];
					#print "Totals=".$Totals{$data[1]}.",\tDbValue=".$DbValue."\n";
					#if($Totals{$data[1]} ne $DbValue) {
					if(! defined($data[4])) {
						print " - UPDATE timetrack SET pttw=$Totals{$data[1]} WHERE id=$data[0]; ( project='$data[1]' data=$data[2] )\n";
						my $up = $dbh->prepare("UPDATE timetrack SET pttw=? WHERE id=?;");
						my $result = $up->execute($Totals{$data[1]}, $data[0]);
					}
				}
				#print "Set previous data\n";
				@previousData = @data;
			}
			# May have started in the middle of the week but we need to continue from the begining of the next week.
			$StartDate->truncate( to => 'week');
			$StartDate->add(weeks => 1);
			$EndDate->add(weeks => 1);
		}
	}
	print "\tDone CalculatePttw\n";
}

# Project Time This Day
sub CalculatePttd {
	print "CalculatePttd\n";
	my $StartDate = DateTime->new(year => 2018, month => 3, day => 1, hour => 0, minute => 0, time_zone => $MyTimeZone);
	$StartDate->truncate( to => 'day');	
	my $sow = $StartDate->epoch();
	my $SqlCmd = "SELECT MAX(time) FROM timetrack WHERE pttd NOT NULL AND project != 'PUNCH';";
	$sow = $dbh->selectall_arrayref($SqlCmd)->[0][0];
	if(! defined $sow) {
		$sow = 0;
	}
	$SqlCmd = "SELECT MIN(time) FROM timetrack WHERE pttd IS NULL AND project != 'PUNCH' AND time > $sow;";
	print "$SqlCmd\n";
	my $StartTimeInt = $dbh->selectall_arrayref($SqlCmd)->[0][0];
	if(defined($StartTimeInt)) {
		print "StartTimeInt=$StartTimeInt\n";
		# We need to start at a pttw that needs to be updated, that is what StartTimeInt is for. Otherwise we end up recalculating all pttw's from Oct 28, 2013
		$StartDate = DateTime::Format::Strptime->new(pattern => '%s', on_error => 'croak')->parse_datetime($StartTimeInt);
		# We need to get the EndDate truncated to the next week.
		$StartDate->truncate( to => 'day');	
		my $EndDate = DateTime->from_object(object => $StartDate);
		$EndDate->truncate( to => 'day');
		$EndDate->add(days => 1);
		$EndDate->subtract(seconds => 1);
		my $Done = 10;
		while ($Done > 0) {
			print "Project Time This Day report from ".$StartDate->day_name()." ".$StartDate->month_name()." ".$StartDate->day().", ".$StartDate->year()."\n";
			my $set = $StartDate->epoch();
			my $StartOfDay = $StartDate;
			$StartOfDay->truncate( to => 'day');
			$sow = $StartOfDay->epoch();
			my $eet = $EndDate->epoch();
			print "Date of ".$StartDate->date()." to ".$EndDate->date()."\n";
			my %Totals = ();
			$SqlCmd = "SELECT id, project, et, activity, pttd FROM timetrack WHERE project != 'PUNCH' AND et NOT NULL AND time >= $set AND time < $eet ORDER BY time;";
			print "$SqlCmd\n";			
			$sth = $dbh->prepare($SqlCmd);
			$sth->execute();
			my @previousData;
			$Done--;
			while (my @data = $sth->fetchrow_array()) {
				$Done = 2;
				# sleep 1;
				print "Working on id=$data[0], project='$data[1]', et=$data[2], pttd=";
				if(defined($data[4])) {
					print "$data[4]\n";
				} else {
					print "NULL\n";
				}
 				if(!defined($Totals{$data[1]})) {
					$SqlCmd = "SELECT MAX(pttd) FROM timetrack WHERE project='$data[1]' AND time > $sow AND time < $eet;";
					print "$SqlCmd\n";					
					$Totals{$data[1]} = $dbh->selectall_arrayref($SqlCmd)->[0][0] or $Totals{$data[1]} = 0;
					print "New project this sample $data[1] set to $Totals{$data[1]}\n";				
				} 
				if(($data[3] eq "Out") and (defined($previousData[0]))) {
					print "+ Totals{$previousData[1]} += $data[2];\n";
					$Totals{$previousData[1]} += $data[2];
					print "+ Totals{$previousData[1]} = $Totals{$previousData[1]};\n";
					#my $DbValue = $dbh->selectall_arrayref("SELECT pttw FROM timetrack WHERE id=".$previousData[0].";")->[0][0];
					#print "Totals=".$Totals{$previousData[1]}.",\tDbValue=".$DbValue."\n";
					#if($Totals{$previousData[1]} ne $DbValue) {
						#print " + UPDATE timetrack SET pttw=$Totals{$previousData[1]} WHERE id=$previousData[0]; ( project='$previousData[1]' data=$data[2] )\n";
						my $up = $dbh->prepare("UPDATE timetrack SET pttd=? WHERE id=?;");
						my $result = $up->execute($Totals{$previousData[1]}, $previousData[0]);
					#}
				} else {
					print "- data[4] = ";
					if(defined($data[4])) {
						print "$data[4]\n";
					} else {
						print "NULL\n";
					}
					print "- Totals{$data[1]} += $data[2];\n";					
					if(! defined($data[4])) {
						$Totals{$data[1]} += $data[2];
						print "- Totals{$data[1]} = $Totals{$data[1]};\n";
						print " - UPDATE timetrack SET pttd=$Totals{$data[1]} WHERE id=$data[0]; ( project='$data[1]' data=$data[2] )\n";
						my $up = $dbh->prepare("UPDATE timetrack SET pttd=? WHERE id=?;");
						my $result = $up->execute($Totals{$data[1]}, $data[0]);
					}
				}
				print "Set previous data\n";
				@previousData = @data;
			}
			# May have started in the middle of the week but we need to continue from the begining of the next week.
			$StartDate->truncate( to => 'day');
			$StartDate->add(days => 1);
			$EndDate->add(days => 1);
		}
	}
	print "\tDone CalculatePttd\n";
}

sub CalculateDst {
	print "CalculateDst\n";
	my $StartDate = DateTime->now->set_time_zone($MyTimeZone);
	$StartDate->truncate(to => 'day');
	# This is to fix yesterdays time if something went wrong
	#$StartDate->subtract(days => 1);
	my $EndDate = DateTime->from_object(object => $StartDate);
	$EndDate->add(days => 1);
	$EndDate->subtract(seconds => 1);
	my $set = $StartDate->epoch();
	my $eet = $EndDate->epoch();
	#print "Day of ".$StartDate->datetime()." to ".$EndDate->datetime()."\n";
	my $Total = 0;
	#print "SELECT id, et, dst FROM timetrack WHERE activity != 'In' AND time > $set AND time < $eet ORDER BY id;\n";
	my $sth = $dbh->prepare("SELECT id, et, dst FROM timetrack WHERE activity != 'In' AND time > $set AND time < $eet ORDER BY id;");
	$sth->execute();
	while (my @data = $sth->fetchrow_array()) {
		if(defined $data[1]) {
			$Total += $data[1];
			if(! defined($data[2])) {
				#print "id=$data[0]\tdata[1]=$data[1]\tTotal=$Total\n";
				#print " - UPDATE timetrack SET dst=$Total WHERE id=$data[0];\n";
				my $up = $dbh->prepare("UPDATE timetrack SET dst=? WHERE id=?;");
				$up->execute($Total, $data[0]);
			}
		}
	}
	print "\tDone CalculateDst\n";
}

sub CreateDb {
	#print "CreateDb\n";
	if(-e File::HomeDir->my_home."/Documents/LWIDAWT.debug") {
		$Debug = 1;
	}
	$DbFile = File::HomeDir->my_home."/Documents/LWIDAWT.db";
	if($Debug == 1) {
		$DbFile .= "_DEBUG";
	}
	print "DB=$DbFile\n";
	$dbh = DBI->connect("dbi:SQLite:dbname=$DbFile", "", "", {sqlite_use_immediate_transaction => 1, RaiseError =>1}) or die $DBI::errstr;


	my $sth = $dbh->prepare("SELECT name FROM sqlite_master;");
	$sth->execute();
	my $Result = $sth->fetchall_arrayref();
	
	if ( ! ( "timetrack" ~~ $Result )) {
		CreateTimeTrackTable();
	}
	if ( ! ( "reportlog" ~~ $Result )) {
		CreateReportLogTable();
	}
	if ( ! ( "eventlog" ~~ $Result )) {
		CreateEventLogTable();
	}
	if ( ! ( "settings" ~~ $Result )) {
		CreateSettingsTable();
		# Settings();
	}
}

sub GetTimeToday {
	print "GetTimeToday\n";
	my $StartDate = DateTime->now->set_time_zone($MyTimeZone);
	$StartDate->truncate( to => 'day');
	my $EndDate = DateTime->from_object(object => $StartDate);
	$EndDate->add(days => 1);
	$EndDate->subtract(seconds => 1);
	my $set = $StartDate->epoch();
	my $eet = $EndDate->epoch();

	my $DayTotal = 0;
	while ($StartDate < DateTime->now->set_time_zone($MyTimeZone)) {
		$DayTotal = $dbh->selectall_arrayref("SELECT MAX(dst) FROM timetrack WHERE project != 'PUNCH' AND time > $set AND time < $eet;")->[0][0];
		$StartDate->add(days => 1);
		$EndDate->add(days => 1);
		$set = $StartDate->epoch();
		$eet = $EndDate->epoch();
	}
	my $Hours = 0; 
	my $Minutes = 0;
	my $Seconds = 0;
	if (defined($DayTotal)) {
		$Hours = int ($DayTotal / 60 / 60);
		$Minutes = int (($DayTotal - ($Hours * 60 * 60)) / 60);
		$Seconds = ($DayTotal - ($Hours * 60 * 60) - ($Minutes * 60));
		# printf "today %01d:%02d:%02d\n", $Hours, $Minutes, $Seconds;
	}

	return(sprintf "%01d:%02d:%02d", $Hours, $Minutes, $Seconds);
}

sub GetTimeThisWeek {
	print "GetTimeThisWeek\n";
	my $StartDate = DateTime->now->set_time_zone($MyTimeZone);
	$StartDate->truncate( to => 'week');
	my $EndDate = DateTime->from_object(object => $StartDate);
	$EndDate->add(days => 1);
	$EndDate->subtract(seconds => 1);
	my $set = $StartDate->epoch();
	my $eet = $EndDate->epoch();

	my $DayTotal = 0;
	my $WeekTotal = 0;
	while ($StartDate < DateTime->now->set_time_zone($MyTimeZone)) {
		$DayTotal = $dbh->selectall_arrayref("SELECT MAX(dst) FROM timetrack WHERE project != 'PUNCH' AND time > $set AND time < $eet;")->[0][0];

		if ( defined($DayTotal)) {
			$WeekTotal += $DayTotal;
			# print "WeekTotal = $WeekTotal\t DayTotal = $DayTotal\n";
		}
		$StartDate->add(days => 1);
		$EndDate->add(days => 1);
		$set = $StartDate->epoch();
		$eet = $EndDate->epoch();
	}
	my $Hours = 0; 
	my $Minutes = 0;
	my $Seconds = 0;
	if ($WeekTotal > 0) {
		$Hours = int ($WeekTotal / 60 / 60);
		$Minutes = int (($WeekTotal - ($Hours * 60 * 60)) / 60);
		$Seconds = ($WeekTotal - ($Hours * 60 * 60) - ($Minutes * 60));
	}
	return(sprintf "%01d:%02d:%02d", $Hours, $Minutes, $Seconds);
}

sub CreateSettingsTable {
	print "CreateSettingsTable\n";
	my $Date = DateTime->now();
	my $MyEpoch = $Date->epoch();
	$dbh->do("CREATE TABLE settings (name CHAR(50) PRIMARY KEY, value CHAR(500));");
}

sub RecordEvent {
	my $EventMsg = shift;
	print "RecordEvent\n";
	my $Date = DateTime->now();
	my $MyEpoch = $Date->epoch();
	my $NextIndex = $dbh->selectall_arrayref("SELECT MAX(id)+1 FROM eventlog;")->[0][0];

	# Disable the event log to see if this fixes the sync problem.
	# The sync problem is when multiple systems are using the same DB and one wants to send an event, locally it thinks the user is logged out.
	# If the user is logged in on the other end it could stop unison from syncing the DB.
	# This could be solved by checking the last hostname logged in from against systems hostname, if they are different do not record the event.
	# $dbh->prepare("INSERT INTO eventlog (id, time, event) VALUES (?, ?, ?);")->execute($NextIndex, $MyEpoch, $EventMsg);
}

sub CreateEventLogTable {
	print "CreateEventLogTable\n";
	my $Date = DateTime->now();
	my $MyEpoch = $Date->epoch();
	$dbh->do("CREATE TABLE eventlog (id INT, time INT, event CHAR(500));");
	$dbh->do("INSERT INTO eventlog (id, time, event) VALUES (0, $MyEpoch, 'Create eventlog table')");
}

sub CreateReportLogTable {
	print "CreateReportLogTable\n";
	my $Date = DateTime->now();
	my $MyEpoch = $Date->epoch();
	$dbh->do("CREATE TABLE reportlog (id INT, report CHAR(30), lasttime INT);");
	$SqlCmd = "INSERT INTO reportlog (id, report, lasttime) VALUES (?, ?, 0)";
	$dbh->prepare($SqlCmd)->execute("0", "lock");
	$dbh->prepare($SqlCmd)->execute("1", "daily");
	$dbh->prepare($SqlCmd)->execute("2", "monthly");
	$dbh->prepare($SqlCmd)->execute("3", "weekly");
}

sub CreateTimeTrackTable {
	print "CreateTimeTrackTable\n";
	my $NextId = 0;
	my $Date = DateTime->now();
	my $MyEpoch = $Date->epoch();
	$dbh->do("CREATE TABLE timetrack (id INT, project CHAR(30), activity CHAR(500), time INT, et INT, dst INT, pttw INT, pttm INT, pttd INT);");
	foreach (@DefaultProjects) {
		$SqlCmd = "INSERT INTO timetrack (id, project, time, activity) VALUES (?, ?, ?, 'Out')";
		$dbh->prepare($SqlCmd)->execute($NextId, $_, $MyEpoch);
		$NextId++;
	}
	$SqlCmd = "INSERT INTO timetrack (id, project, activity, time) VALUES (?, ?, ?, ?)";
	$dbh->prepare($SqlCmd)->execute($NextId, "PUNCH", "Out", $MyEpoch);
}

sub ResetFailedToEnterCount {
	$dbh->prepare("UPDATE settings SET value='0' WHERE name = 'FailedToEnterCounter';")->execute;
}



1;
