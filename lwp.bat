@echo off
REM If using Cygwin, the DB file is in C:\Cygwin\home\<USER>\Documents\LWIDAWT.db

REM Change this to be the location where the code was checked out
cd C:\Users\Royce\Devel\Subversion\TimeTracker

REM For use with Cygwin Perl
C:\cygwin64\bin\run -p /bin/ perl ./LookWhatIDidAtWorkToday.pl %*


REM Use the timeout to see errors
REM timeout 15