#!/bin/bash

[ "${DISPLAY}x" = "x" ] && {
	# When cron calls the program, ${DISPLAY} is blank
	DISPLAY=$(echo "SELECT value FROM settings WHERE name='DISPLAY';" | sqlite3 ~/Documents/LWIDAWT.db)
	true
} || {
	# When a user calls the program, like when they first login in the morning, ${DISPLAY} has a value.	
	echo "INSERT OR IGNORE INTO settings VALUES ('DISPLAY', '$DISPLAY'); UPDATE settings SET value='$DISPLAY' WHERE name='DISPLAY';" | sqlite3 ~/Documents/LWIDAWT.db
}

~/perl5/perlbrew/bin/perlbrew exec ${0//osx.sh/LookWhatIDidAtWorkToday.pl} $@

